#pragma once

#define ensure(x) if(!(x)){throw "ensure failed: "#x"";}

// Standard platform types
typedef unsigned long long int uint64;
typedef unsigned int           uint32;
typedef unsigned short int     uint16;
typedef unsigned char           uint8;
typedef signed long long int    int64;
typedef signed int              int32;
typedef signed short int        int16;
typedef signed char              int8;

// DLL helper definition
#ifdef VG_PLATFORM_WINDOWS
	#ifdef VG_BUILD_DLL
		#define VEGA_API __declspec(dllexport)
		#define VEGA_API_EXPORTONLY __declspec(dllexport)
		#define VG_LOG(LEVEL, ...) if(LEVEL){if(LEVEL-1){Vega::EngineLog->Error(__VA_ARGS__);}else{Vega::EngineLog->Warn(__VA_ARGS__);}}else{Vega::EngineLog->Info(__VA_ARGS__);}
		#define VG_LOGR(LEVEL, ...) if(LEVEL){if(LEVEL-1){Vega::EngineLog->Error_Raw(__VA_ARGS__);}else{Vega::EngineLog->Warn_Raw(__VA_ARGS__);}}else{Vega::EngineLog->Info_Raw(__VA_ARGS__);}

	#else
		#define VEGA_API __declspec(dllimport)
		#define VEGA_API_EXPORTONLY
		#define VG_LOG(LEVEL, ...) if(LEVEL){if(LEVEL-1){Vega::ClientLog->Error(__VA_ARGS__);}else{Vega::ClientLog->Warn(__VA_ARGS__);}}else{Vega::ClientLog->Info(__VA_ARGS__);}
		#define VG_LOGR(LEVEL, ...) if(LEVEL){if(LEVEL-1){Vega::ClientLog->Error_Raw(__VA_ARGS__);}else{Vega::ClientLog->Warn_Raw(__VA_ARGS__);}}else{Vega::ClientLog->Info_Raw(__VA_ARGS__);}

	#endif

#else 
	#error Vega only supports Windows!
#endif