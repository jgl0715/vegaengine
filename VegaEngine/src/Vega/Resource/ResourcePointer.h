#pragma once

#include "../Core.h"

namespace Vega
{

	class Resource;

	template<class T>
	class VEGA_API ResPtr
	{
	public:
		ResPtr() { Res = nullptr; };
		~ResPtr() {};

		T& operator*() { return *Res; }
		T& operator=(T* Res) { this->Res = Res; };

	private:
		T* Res;
	};
}

