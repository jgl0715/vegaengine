#pragma once

#include "../Core.h"
#include "ResourcePointer.h"
#include <string>
#include <unordered_map>

namespace Vega
{

	class Texture;
	class Resource;
	class BitmapFont;

	class VEGA_API ResourceManager
	{
	public:
		ResourceManager();
		virtual ~ResourceManager();

		template<class T>
		Vega::Resource* Get(const std::string& ResourcePath);

		Vega::Texture* GetTexture(const std::string& ResourcePath);
		Vega::BitmapFont* GetFont(const std::string& ResourcePath);

		void FreeAll();

	private:
		std::unordered_map<std::string, Vega::Resource*>* Resources;
	};


}
