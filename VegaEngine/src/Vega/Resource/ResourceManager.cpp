#include "ResourceManager.h"
#include "Resource.h"
#include "../Global.h"
#include "../Rendering/GL/GLTexture.h"
#include "../Rendering/GUI/BitmapFont.h"

namespace Vega
{

	ResourceManager::ResourceManager()
	{
		Resources = new std::unordered_map<std::string, Vega::Resource*> ();
	}

	ResourceManager::~ResourceManager()
	{
		delete Resources;
	}

	template<class T>
	Vega::Resource* ResourceManager::Get(const std::string& ResourcePath)
	{
		std::unordered_map<std::string, Vega::Resource*>::iterator FoundLoc = Resources->find(ResourcePath);

		if (FoundLoc != Resources->end())
		{
			// Resource exists, simply return it.
			return FoundLoc->second;
		}
		else
		{
			// Have to create and load the resource.
			T* Resource = new T;

			// Loading is done synchronously for now.
			// TODO: add asynchronous loading capabilities
			Resource->Load(ResourcePath);

			Resources->insert(std::pair<std::string, Vega::Resource*>(ResourcePath, Resource));
			
			return Resource;
		}
	}

	Vega::Texture* ResourceManager::GetTexture(const std::string& ResourcePath)
	{
		Vega::Texture* ResultTexture = nullptr;

		if (Vega::Graphics::RenderingAPI == Vega::Graphics::RAPI::OPENGL4)
		{
			ResultTexture = static_cast<Vega::Texture*>(Get<Vega::GLTexture>(ResourcePath));
		}

		return ResultTexture;
	}

	Vega::BitmapFont* ResourceManager::GetFont(const std::string& ResourcePath)
	{
		return static_cast<Vega::BitmapFont*>(Get<Vega::BitmapFont>(ResourcePath));
	}

	void ResourceManager::FreeAll()
	{
		std::unordered_map<std::string, Vega::Resource*>::iterator Itr = Resources->begin();

		while (Itr != Resources->end())
		{
			// Free the resource.
			Itr->second->Free();

			++Itr;
		}
	}

}

