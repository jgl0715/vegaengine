#pragma once

#include "../Core.h"
#include <string>

namespace Vega
{

	class VEGA_API Resource
	{
	public:
		Resource() {};
		virtual ~Resource() {};
		
		virtual void Load(const std::string& Path) = 0;
		virtual void Free() = 0;
		
	private:

		uint32 References;
	};

}
