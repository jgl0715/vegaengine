#include "Path.h"


namespace Vega
{

	Path::Path(const Vega::String& Path)
	{
		char PathSeparator = '/';
		int32 LastSeparator = -1;
		int32 NextSeparator = Path.Find(PathSeparator);

		PathStrings = new std::vector<Vega::String>();

		while (NextSeparator >= 0)
		{
			Vega::String PathElement = Path.Substring(LastSeparator + 1, NextSeparator);

			PathStrings->push_back(PathElement);

			LastSeparator = NextSeparator;
			NextSeparator = Path.Find(PathSeparator, NextSeparator + 1);
		}

		PathStrings->push_back(Path.Substring(LastSeparator + 1, Path.GetSize()));

	}

	Path::Path(const std::vector<Vega::String> PathElements)
	{
		std::vector<Vega::String>::iterator PathItr;

		PathStrings = new std::vector<Vega::String>();
		
		while (PathItr != PathElements.end())
		{
			PathStrings->push_back(*PathItr);
			PathItr++;
		}
	}

	Path::~Path()
	{
		delete PathStrings;
	}

	Vega::String Path::GetFileName()
	{
		return *PathStrings->end();
	}

	Vega::String Path::GetParentName()
	{
		if (PathStrings->size() >= 2)
			return PathStrings->at(PathStrings->size() - 2);
		else
			return "";
	}

	Vega::Path Path::GetParent()
	{
		std::vector<Vega::String> ParentElements;

		for (uint32 Index = 0; Index < PathStrings->size() - 1; Index++)
			ParentElements.push_back(PathStrings->at(Index));

		return Vega::Path(ParentElements);
	}
	
	const Vega::String& Path::ToString()
	{
		return "";
	}

}
