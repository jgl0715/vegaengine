#include "Log.h"
#include <windows.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctime>

namespace Vega
{

	Vega::Log* ClientLog;
	Vega::Log* EngineLog;


#ifdef VG_PLATFORM_WINDOWS

	void Colorize(LogColor Color)
	{
		HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
		WORD wOldColorAttrs;
		CONSOLE_SCREEN_BUFFER_INFO csbiInfo;

		// Save attribute state
		GetConsoleScreenBufferInfo(h, &csbiInfo);
		wOldColorAttrs = csbiInfo.wAttributes;

		switch (Color)
		{
		case LogColor::BLUE:
			SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			break;
		case LogColor::GREEN:
			SetConsoleTextAttribute(h, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			break;
		case LogColor::CYAN:
			SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			break;
		case LogColor::RED:
			SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_INTENSITY);
			break;
		case LogColor::MAGENTA:
			SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			break;
		case LogColor::YELLOW:
			SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			break;
		case LogColor::WHITE:
			SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			break;
		case LogColor::GRAY:
			SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		}

	}

#endif

	Log::Log(const std::string& Name)
	{
		InfoColor = LogColor::GRAY;
		WarnColor = LogColor::YELLOW;
		ErrorColor = LogColor::RED;

		OutputStream = stdout;

		this->LoggerName = new std::string(Name);
	}

	Log::~Log()
	{
		delete this->LoggerName;
	}

	LogColor Log::GetInfoColor() const
	{
		return InfoColor;
	}

	LogColor Log::GetWarnColor() const
	{
		return WarnColor;
	}

	LogColor Log::GetErrorColor() const
	{
		return ErrorColor;
	}

	void Log::SetInfoColor(LogColor Color)
	{
		this->InfoColor = Color;
	}

	void Log::SetWarnColor(LogColor Color)
	{
		this->WarnColor = Color;
	}

	void Log::SetErrorColor(LogColor Color)
	{
		this->ErrorColor = Color;
	}

	void Log::Output(LogColor Color, std::string Format, va_list VariadicList) const
	{
		Colorize(Color);

		std::time_t Time = std::time(0);
		std::tm Now;
		localtime_s(&Now, &Time);

		// [Hours:Minutes:Seconds][LoggerName]:
		fprintf(OutputStream, "[%02d:%02d:%02d][%s]: ", Now.tm_hour, Now.tm_min, Now.tm_sec, LoggerName->c_str());

		vfprintf(OutputStream, Format.c_str(), VariadicList);
		fprintf(OutputStream, "\n");
	}

	void Log::Output_Raw(LogColor Color, std::string Format, va_list VariadicList) const
	{
		// Output log without prefix (raw)

		Colorize(Color);

		vfprintf(OutputStream, Format.c_str(), VariadicList);
		fprintf(OutputStream, "\n");
	}

	void Log::Info(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output(InfoColor, Format, ArgsList);
	}

	void Log::Warn(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output(WarnColor, Format, ArgsList);
	}

	void Log::Error(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output(ErrorColor, Format, ArgsList);
	}

	void Log::Info_Raw(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output_Raw(InfoColor, Format, ArgsList);
	}
	
	void Log::Warn_Raw(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output_Raw(WarnColor, Format, ArgsList);
	}
	
	void Log::Error_Raw(std::string Format, ...) const
	{
		va_list ArgsList;
		va_start(ArgsList, Format);

		Output_Raw(ErrorColor, Format, ArgsList);
	}

}


