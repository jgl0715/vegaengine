#pragma once

#include "../Core.h"

namespace Vega
{
	class VEGA_API ApplicationListener
	{
	public:
		ApplicationListener();
		virtual ~ApplicationListener();

		virtual void Init();
		virtual void Update();
		virtual void Render();
		virtual void Quit();

	};

	ApplicationListener* CreateApplicationListener();
}
