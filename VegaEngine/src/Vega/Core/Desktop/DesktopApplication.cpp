#include "DesktopApplication.h"
#include "Window.h"
#include "../ApplicationListener.h"
#include "../Log.h"

#include "GLFW/glfw3.h"
#include <iostream>
#include <chrono>
#include <thread>

namespace Vega
{

	DesktopApplication::DesktopApplication(ApplicationListener* App)
	{
		this->App = App;
		this->Running = false;
		this->Window = new Vega::Window;
		this->FramesPerSecond = 0;
		this->UpdatesPerSecond = 0;
		this->FrameCounter = 0;
		this->UpdateCounter= 0;
	}

	DesktopApplication::~DesktopApplication()
	{
		delete Window;
	}

	bool DesktopApplication::IsRunning()
	{
		return Running;
	}


	Vega::ApplicationListener* DesktopApplication::GetAppListener()
	{
		return App;
	}
	
	Vega::Window* DesktopApplication::GetWindow()
	{
		return Window;
	}

	int DesktopApplication::GetFps()
	{
		return UpdatesPerSecond;
	}

	int DesktopApplication::GetUps()
	{
		return FramesPerSecond;
	}

	void DesktopApplication::Init()
	{
		if (!glfwInit())
		{
			throw "GLFW failed to init!";
		}
		else
		{
			// Initialize the standard loggers
			EngineLog = new Vega::Log("VegaLog");
			ClientLog = new Vega::Log("ClientLog");

			Window->Create(16*60,9*60, "Engine");
			
			App->Init();
		}
	}

	void DesktopApplication::Update()
	{
		glfwPollEvents();

		App->Update();

		if (Window->IsCloseRequested())
		{
			Stop();
		}
	}

	void DesktopApplication::Render()
	{
		// TODO: Clear the screen to a specified clear color
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		App->Render();

		/* Swap front and back buffers */
		Window->SwapBuffers();
	}

	void DesktopApplication::Quit()
	{
		// Free the standard loggers
		delete EngineLog;
		delete ClientLog;

		App->Quit();
	}

	void DesktopApplication::Launch()
	{
		Running = true;

		Init();
	
		auto Current      = std::chrono::high_resolution_clock::now();
		auto LastUpdate   = std::chrono::high_resolution_clock::now();
		auto LastFrame    = std::chrono::high_resolution_clock::now();
		auto LastFPSQuery = std::chrono::high_resolution_clock::now();

		std::chrono::nanoseconds DeltaUpdate;
		std::chrono::nanoseconds DeltaRender;
		std::chrono::nanoseconds DeltaFPSQuery;

		double DeltaSecondsUpdate;
		double DeltaSecondsRender;
		double DeltaSecondsFPSQuery;
		double RemainingTimeUpdate;

		int32 SleepTime;

		while (Running)
		{
			Current = std::chrono::high_resolution_clock::now();

			DeltaUpdate   = std::chrono::duration_cast<std::chrono::nanoseconds>(Current - LastUpdate);
			DeltaRender   = std::chrono::duration_cast<std::chrono::nanoseconds>(Current - LastFrame);
			DeltaFPSQuery = std::chrono::duration_cast<std::chrono::nanoseconds>(Current - LastFPSQuery);

			DeltaSecondsUpdate   = DeltaUpdate.count() / (double)1e9;
			DeltaSecondsRender   = DeltaRender.count() / (double)1e9;
			DeltaSecondsFPSQuery = DeltaFPSQuery.count() / (double)1e9;

			RemainingTimeUpdate = FRAME_TIME - DeltaSecondsUpdate;

			if (DeltaSecondsUpdate >= UPDATE_TIME)
			{
				Update();
				UpdateCounter++;

				LastUpdate = std::chrono::high_resolution_clock::now();
			}

			if (DeltaSecondsRender >= FRAME_TIME)
			{
				Render();
				FrameCounter++;

				LastFrame = std::chrono::high_resolution_clock::now();
			}

			if (DeltaSecondsFPSQuery >= 1.0)
			{

				UpdatesPerSecond = UpdateCounter;
				FramesPerSecond = FrameCounter;

				FrameCounter = 0;
				UpdateCounter = 0;

				LastFPSQuery = std::chrono::high_resolution_clock::now();
			}

			// Sleep if there is enough remaining time
			if (RemainingTimeUpdate > 0.001f)
			{
				SleepTime = static_cast<int32>(RemainingTimeUpdate * 1000) - 1;
				std::this_thread::sleep_for(std::chrono::milliseconds(SleepTime));
			}

		}

		Quit();
	}

	void DesktopApplication::Stop()
	{
		Running = false;
	}

}