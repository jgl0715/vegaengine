#pragma once

#include "../../Core.h"

#define FRAME_LOCK 60.0
#define UPDATE_LOCK 60.0
#define UPDATE_TIME (1.0 / FRAME_LOCK)
#define FRAME_TIME (1.0 / FRAME_LOCK)

namespace Vega
{

	class ApplicationListener;
	class Window;

	class VEGA_API DesktopApplication
	{
	public:
		DesktopApplication(ApplicationListener* App);
		~DesktopApplication();

		bool IsRunning();

		void Launch();
		void Stop();

		Vega::ApplicationListener* GetAppListener();
		Vega::Window* GetWindow();
		int GetFps();
		int GetUps();

	private:

		void Init();
		void Update();
		void Render();
		void Quit();

		bool Running;
		Vega::ApplicationListener* App;
		Vega::Window* Window;

		int UpdatesPerSecond;
		int FramesPerSecond;

		int FrameCounter;
		int UpdateCounter;

	};
}


