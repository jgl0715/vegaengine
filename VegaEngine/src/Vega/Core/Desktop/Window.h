#pragma once

#include "../../Core.h"

struct GLFWwindow;

namespace Vega
{
	class Window
	{
	public:
		Window();
		~Window();

		void Create(int Width, int Height, const char* Title);

		void SwapBuffers();

		int32 GetWidth();
		int32 GetHeight();

		bool IsCloseRequested();

	private:

		GLFWwindow* Handle;

	};

}
