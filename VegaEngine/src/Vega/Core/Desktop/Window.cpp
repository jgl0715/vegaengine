#include "Window.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"

namespace Vega
{
	Window::Window()
	{
	}

	Window::~Window()
	{
	}

	void Window::Create(int Width, int Height, const char* Title)
	{

		//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		Handle = glfwCreateWindow(Width, Height, Title, NULL, NULL);

		if (!Handle)
		{
			throw "Failed to create GLFW window!";
		}
		else
		{

			// Get the primary monitor's video mode.
			const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window at the middle of the screen.
			glfwSetWindowPos(Handle, (vidmode->width - Width) / 2, (vidmode->height - Height) / 2);

			// Make the window's context current
			glfwMakeContextCurrent(Handle);
			glewExperimental = GL_TRUE;

			// Initialize GLEW
			if (glewInit() != GLEW_OK)
			{
				throw "Failed to initialize GLEW!";
			}

			//glfwSetKeyCallback(window, &key_callback);
		}
	}

	void Window::SwapBuffers()
	{
		glfwSwapBuffers(Handle);
	}


	int32 Window::GetWidth()
	{
		int Width, Height;
		glfwGetWindowSize(Handle, &Width, &Height);

		return Width;
	}
	
	int32 Window::GetHeight()
	{
		int Width, Height;
		glfwGetWindowSize(Handle, &Width, &Height);

		return Height;

	}

	bool Window::IsCloseRequested()
	{
		return glfwWindowShouldClose(Handle);
	}
}
