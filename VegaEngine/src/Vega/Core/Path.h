#pragma once

#include "String.h"
#include <vector>

namespace Vega
{
	class Path
	{
	public:
		Path(const Vega::String& Path);
		Path(const std::vector<Vega::String> PathElements);
		~Path();

		Vega::String GetFileName();
		Vega::String GetParentName();
		Vega::Path GetParent();
		const Vega::String& ToString();

	private:

		std::vector<Vega::String>* PathStrings;
		
	};

}

