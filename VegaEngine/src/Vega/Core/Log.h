#pragma once

#include <string>
#include "../Core.h"

#define LOG_INFO  0
#define LOG_WARN  1
#define LOG_ERROR 2

namespace Vega
{
	
	enum class VEGA_API LogColor : uint16
	{
		BLUE,
		GREEN,
		CYAN,
		RED,
		MAGENTA,
		YELLOW,
		WHITE,
		GRAY,
	};

	void Colorize(LogColor Color);

	class VEGA_API Log
	{
	public:
		Log(const std::string& Name);
		~Log();
		
		LogColor GetInfoColor() const;
		LogColor GetWarnColor() const;
		LogColor GetErrorColor() const;

		void SetInfoColor(LogColor Color);
		void SetWarnColor(LogColor Color);
		void SetErrorColor(LogColor Color);

		void Info(std::string Format, ...) const;
		void Warn(std::string Format, ...) const;
		void Error(std::string Format, ...) const;

		void Info_Raw(std::string Format, ...) const;
		void Warn_Raw(std::string Format, ...) const;
		void Error_Raw(std::string Format, ...) const;

	private:

		void Output(LogColor Color, std::string Format, va_list VariadicList) const;
		void Output_Raw(LogColor Color, std::string Format, va_list VariadicList) const;

		FILE* OutputStream;

		std::string* LoggerName;

		LogColor InfoColor;
		LogColor WarnColor;
		LogColor ErrorColor;
	};

	extern VEGA_API Vega::Log* ClientLog;
	extern VEGA_API Vega::Log* EngineLog;

}
