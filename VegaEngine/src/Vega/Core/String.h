#pragma once

#include "../Core.h"
#include <string>

namespace Vega
{
	class String
	{
	public:
	
		String();
		String(const char Data[]);
		~String();

		int32 Find(char Element) const;
		int32 Find(char Element, uint32 From) const;
		int32 FindLast(char Element) const;
		Vega::String Append(const Vega::String& Other) const;
		Vega::String Substring(uint32 Start, uint32 End) const;

		uint32 GetSize() const;

		char operator[](uint32 Index) const;
		void operator=(const Vega::String& Other);
		void operator=(const char StdString[]);
	
	private:

		char* Data;
		uint32 Size;

	};
}
