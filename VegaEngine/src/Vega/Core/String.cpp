#include "String.h"
#include <string.h>

namespace Vega
{

	String::String()
	{
		Data = nullptr;
	}

	String::String(const char Data[])
	{
		this->Size = static_cast<uint32>(strlen(Data));
		this->Data = new char[Size+1];

		for (uint32 Index = 0; Index < Size; Index++)
			this->Data[Index] = Data[Index];

		this->Data[Size] = '\0';
	}

	String::~String()
	{
		delete Data;
	}

	int32 String::Find(char Element) const
	{
		for (uint32 Index = 0; Index < Size; Index++)
		{
			if (Data[Index] == Element)
				return Index;
		}
		return -1;
	}

	int32 String::Find(char Element, uint32 From) const
	{
		for (uint32 Index = From; Index < Size; Index++)
		{
			if (Data[Index] == Element)
				return Index;
		}
		return -1;
	}

	int32 String::FindLast(char Element) const
	{
		for (int32 Index = Size - 1; Index >= 0; Index--)
		{
			if (Data[Index] == Element)
				return Index;
		}
		return -1;
	}

	Vega::String String::Substring(uint32 Start, uint32 End) const
	{
		ensure(End >= Start);

		int32 SubstringSize = End - Start + 1;
		char* Result = new char[SubstringSize];

		for (uint32 Index = Start; Index < End; Index++)
		{
			Result[Index - Start] = Data[Index];
		}

		// Null-terminate the substring.
		Result[SubstringSize] = '\0';

		return Vega::String(Data);
	}

	Vega::String String::Append(const Vega::String& Other) const
	{
		uint32 Index;
		Vega::String NewString;

		// Calculate new string stats.
		NewString.Size = Size + Other.Size;
		NewString.Data = new char[NewString.Size + 1];

		// Copy over the contents of this string into the new string.
		for (Index = 0; Index < Size; Index++)
			NewString.Data[Index] = Data[Index];

		// Copy over the contents of the other string into the new string.
		for (Index = 0; Index < Other.Size; Index++)
			NewString.Data[Index+NewString.Size] = Other.Data[Index];
		
		// Null terminate the string.
		NewString.Data[NewString.Size] = '\0';
		
		return NewString;
	}

	uint32 String::GetSize() const
	{
		return Size;
	}

	char String::operator[](uint32 Index) const
	{
		return Data[Index];
	}

	void String::operator=(const Vega::String& Other)
	{
		// Free the current data if there is any remaining.
		if (Data != nullptr)
			delete Data;

		// Copy over stats from the other string.
		this->Size = Other.Size;
		this->Data = new char[Size + 1];

		// Copy the data from the other string over into this string.
		for (uint32 Index = 0; Index < Size; Index++)
			Data[Index] = Other[Index];

		// Null terminate the string.
		Data[Size] = '\0';
	}

	void String::operator=(const char StdString[])
	{
		// Free the current data if there is any remaining.
		if (Data != nullptr)
			delete Data;

		// Copy over stats from the other string.
		this->Size = static_cast<uint32>(strlen(StdString));
		this->Data = new char[Size + 1];

		// Copy the data from the other string over into this string.
		for (uint32 Index = 0; Index < Size; Index++)
			Data[Index] = StdString[Index];

		// Null terminate the string.
		Data[Size] = '\0';
	}

}

