#include "Vector.h"

#include <iomanip>
#include <math.h>

namespace Vega
{

	// ---------- Vec4 Definitions -------------------
	Vec4::Vec4() : X(0.0f), Y(0.0f), Z(0.0f), W(0.0f)
	{
	}

	Vec4::Vec4(const Vega::Vec4& Copy) : X(Copy.X), Y(Copy.Y), Z(Copy.Z), W(Copy.W)
	{
	}

	Vec4::Vec4(float VecX, float VecY, float VecZ, float VecW) : X(VecX), Y(VecY), Z(VecZ), W(VecW)
	{
	}

	Vec4::~Vec4()
	{
	}

	Vega::Vec4 Vec4::Scale(float Scalar)
	{
		return Vec4(this->X * Scalar, this->Y * Scalar, this->Z * Scalar, this->W * Scalar);
	}

	Vega::Vec4 Vec4::Add(const Vega::Vec4& Other)
	{
		return Vega::Vec4(this->X + Other.X, this->Y + Other.Y, this->Z + Other.Z, this->W + Other.W);
	}

	Vega::Vec4 Vec4::Subtract(const Vega::Vec4& Other)
	{
		return Vega::Vec4(this->X - Other.X, this->Y - Other.Y, this->Z - Other.Z, this->W - Other.W);
	}

	void Vec4::Scale_M(float Scalar)
	{
		this->X *= Scalar;
		this->Y *= Scalar;
		this->Z *= Scalar;
		this->W *= Scalar;
	}

	void Vec4::Add_M(const Vega::Vec4& Other)
	{
		this->X += Other.X;
		this->Y += Other.Y;
		this->Z += Other.Z;
		this->W += Other.W;
	}

	void Vec4::Subtract_M(const Vega::Vec4& Other)
	{
		this->X -= Other.X;
		this->Y -= Other.Y;
		this->Z -= Other.Z;
		this->W -= Other.W;
	}

	float Vec4::Magnitude()
	{
		return sqrtf(X * X + Y * Y + Z * Z + W * W);
	}

	float Vec4::Dot(const Vega::Vec4& Other)
	{
		return X * Other.X + Y * Other.Y + Z * Other.Z + W * Other.W;
	}

	float Vec4::MagnitudeSquared()
	{
		return X * X + Y * Y + Z * Z + W * W;
	}

	Vega::Vec4 operator*(const Vega::Vec4& Vec, float Scalar)
	{
		return Vec4(Vec.X * Scalar, Vec.Y * Scalar, Vec.Z * Scalar, Vec.W * Scalar);
	}

	Vega::Vec4 operator*(float Scalar, const Vega::Vec4& Vec)
	{
		return Vec4(Vec.X * Scalar, Vec.Y * Scalar, Vec.Z * Scalar, Vec.W * Scalar);
	}

	Vega::Vec4 operator+(const Vega::Vec4& Left, const Vega::Vec4& Right)
	{
		return Vega::Vec4(Left.X + Right.X, Left.Y + Right.Y, Left.Z + Right.Z, Left.W + Right.W);
	}

	Vega::Vec4 operator-(const Vega::Vec4& Left, const Vega::Vec4& Right)
	{
		return Vega::Vec4(Left.X - Right.X, Left.Y - Right.Y, Left.Z - Right.Z, Left.W - Right.W);
	}

	Vega::Vec4 operator*=(Vega::Vec4& Left, float Scalar)
	{
		Left.Scale_M(Scalar);
		return Left;
	}

	Vega::Vec4 operator+=(Vega::Vec4& Left, const Vega::Vec4& Right)
	{
		Left.Add_M(Right);
		return Left;
	}

	Vega::Vec4 operator-=(Vega::Vec4& Left, const Vega::Vec4& Right)
	{
		Left.Subtract_M(Right);
		return Left;
	}

	std::ostream& operator<<(std::ostream& ost, const Vega::Vec4& Vec)
	{
		ost << std::fixed << std::setprecision(2) << "[" << Vec.X << ", " << Vec.Y << ", " << Vec.Z << ", " << Vec.W << "]" << std::endl;
		return ost;
	}

	float& Vec4::operator[](int Index)
	{
		switch (Index)
		{
		case 0:
			return X;
			break;
		case 1:
			return Y;
			break;
		case 2:
			return Z;
			break;
		case 3:
			return W;
			break;
		default:
			throw std::out_of_range("Vec3 access must be one of 0, 1, or 2");
			break;
		}
	}


	// ---------- Vec3 Definitions -------------------

	Vec3::Vec3() : X(0.0f), Y(0.0f), Z(0.0f) 
	{
	}

	Vec3::Vec3(const Vega::Vec3& Copy): X(Copy.X), Y(Copy.Y), Z(Copy.Z)
	{
	}

	Vec3::Vec3(float VecX, float VecY, float VecZ): X(VecX), Y(VecY), Z(VecZ)
	{
	}

	Vec3::~Vec3()
	{
	}

	Vega::Vec3 Vec3::Scale(float Scalar)
	{
		return Vec3(this->X * Scalar, this->Y * Scalar, this->Z * Scalar);
	}

	Vega::Vec3 Vec3::Add(const Vega::Vec3& Other)
	{
		return Vega::Vec3(this->X + Other.X, this->Y + Other.Y, this->Z + Other.Z);
	}

	Vega::Vec3 Vec3::Subtract(const Vega::Vec3& Other)
	{
		return Vega::Vec3(this->X - Other.X, this->Y - Other.Y, this->Z - Other.Z);
	}

	void Vec3::Scale_M(float Scalar)
	{
		this->X *= Scalar;
		this->Y *= Scalar;
		this->Z *= Scalar;
	}
	
	void Vec3::Add_M(const Vega::Vec3& Other)
	{
		this->X += Other.X;
		this->Y += Other.Y;
		this->Z += Other.Z;
	}

	void Vec3::Subtract_M(const Vega::Vec3& Other)
	{
		this->X -= Other.X;
		this->Y -= Other.Y;
		this->Z -= Other.Z;
	}

	float Vec3::Magnitude()
	{
		return sqrtf(X * X + Y * Y + Z * Z);
	}

	float Vec3::Dot(const Vega::Vec3& Other)
	{
		return X * Other.X + Y * Other.Y + Z * Other.Z;
	}
	
	float Vec3::MagnitudeSquared()
	{
		return X * X + Y * Y + Z * Z;
	}

	Vega::Vec3 operator*(const Vega::Vec3& Vec, float Scalar)
	{
		return Vec3(Vec.X * Scalar, Vec.Y * Scalar, Vec.Z * Scalar);
	}

	Vega::Vec3 operator*(float Scalar, const Vega::Vec3& Vec)
	{
		return Vec3(Vec.X * Scalar, Vec.Y * Scalar, Vec.Z * Scalar);
	}

	Vega::Vec3 operator+(const Vega::Vec3& Left, const Vega::Vec3& Right)
	{
		return Vega::Vec3(Left.X + Right.X, Left.Y + Right.Y, Left.Z + Right.Z);
	}
	
	Vega::Vec3 operator-(const Vega::Vec3& Left, const Vega::Vec3& Right)
	{
		return Vega::Vec3(Left.X - Right.X, Left.Y - Right.Y, Left.Z - Right.Z);
	}

	Vega::Vec3 operator*=(Vega::Vec3& Left, float Scalar)
	{
		Left.Scale_M(Scalar);
		return Left;
	}

	Vega::Vec3 operator+=(Vega::Vec3& Left, const Vega::Vec3& Right)
	{
		Left.Add_M(Right);
		return Left;
	}
	
	Vega::Vec3 operator-=(Vega::Vec3& Left, const Vega::Vec3& Right)
	{
		Left.Subtract_M(Right);
		return Left;
	}

	std::ostream& operator<<(std::ostream& ost, const Vega::Vec3& Vec)
	{
		ost << std::fixed << std::setprecision(2) << "[" << Vec.X << ", " << Vec.Y << ", " << Vec.Z << "]" << std::endl;
		return ost;
	}

	float& Vec3::operator[](int Index)
	{
		switch (Index)
		{
		case 0:
			return X;
			break;
		case 1:
			return Y;
			break;
		case 2:
			return Z;
			break;
		default:
			throw std::out_of_range("Vec3 access must be one of 0, 1, or 2");
			break;
		}
	}

	// ---------- Vec2 Definitions -------------------

	Vec2::Vec2() : X(0.0f), Y(0.0f)
	{
	}

	Vec2::Vec2(const Vega::Vec2& Copy): X(Copy.X), Y(Copy.Y)
	{
	}

	Vec2::Vec2(float VecX, float VecY) : X(VecX), Y(VecY)
	{

	}

	Vec2::~Vec2()
	{

	}

	Vega::Vec2 Vec2::Scale(float Scalar)
	{
		return Vega::Vec2(this->X * Scalar, this->Y * Scalar);
	}

	Vega::Vec2 Vec2::Add(const Vega::Vec2& Other)
	{
		return Vega::Vec2(this->X + Other.X, this->Y + Other.Y);
	}

	Vega::Vec2 Vec2::Subtract(const Vega::Vec2& Other)
	{
		return Vega::Vec2(this->X - Other.X, this->Y - Other.Y);
	}

	void Vec2::Scale_M(float Scalar)
	{
		this->X *= Scalar;
		this->Y *= Scalar;
	}

	void Vec2::Add_M(const Vega::Vec2& Other)
	{
		this->X += Other.X;
		this->Y += Other.Y;
	}

	void Vec2::Subtract_M(const Vega::Vec2& Other)
	{
		this->X -= Other.X;
		this->Y -= Other.Y;
	}

	float Vec2::Magnitude()
	{
		return sqrtf(X * X + Y * Y);
	}

	float Vec2::Dot(const Vega::Vec2& Other)
	{
		return X * Other.X + Y * Other.Y;
	}

	float Vec2::MagnitudeSquared()
	{
		return X * X + Y * Y;
	}

	float& Vec2::operator[](int Index)
	{
		switch (Index)
		{
		case 0:
			return X;
			break;
		case 1:
			return Y;
			break;
		default:
			throw std::out_of_range("Vec2 access must be either 0 or 1");
			break;
		}
	}

	Vega::Vec2 operator*(const Vega::Vec2& Vec, float Scalar)
	{
		return Vec2(Vec.X * Scalar, Vec.Y * Scalar);
	}

	Vega::Vec2 operator*(float Scalar, const Vega::Vec2& Vec)
	{
		return Vec2(Vec.X * Scalar, Vec.Y * Scalar);
	}

	Vega::Vec2 operator+(const Vega::Vec2& Left, const Vega::Vec2& Right)
	{
		return Vec2(Left.X + Right.X, Left.Y + Right.Y);
	}

	Vega::Vec2 operator-(const Vega::Vec2& Left, const Vega::Vec2& Right)
	{
		return Vec2(Left.X - Right.X, Left.Y - Right.Y);
	}

	Vega::Vec2 operator*=(Vega::Vec2& Left, float Scalar)
	{
		Left.Scale_M(Scalar);
		return Left;
	}

	Vega::Vec2 operator+=(Vega::Vec2& Left, const Vega::Vec2& Right)
	{
		Left.Add_M(Right);
		return Left;
	}

	Vega::Vec2 operator-=(Vega::Vec2& Left, const Vega::Vec2& Right)
	{
		Left.Subtract_M(Right);
		return Left;
	}

	std::ostream& operator<<(std::ostream& ost, const Vega::Vec2& Vec)
	{
		ost << std::fixed << std::setprecision(2) << "[" << Vec.X << ", " << Vec.Y << "]";
		return ost;
	}

}

