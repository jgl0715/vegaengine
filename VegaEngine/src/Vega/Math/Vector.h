#pragma once

#include <iostream>
#include "../Core.h"

namespace Vega
{
	class VEGA_API Vec4
	{
	public:
		Vec4();
		Vec4(const Vega::Vec4& Copy);
		Vec4(float VecX, float VecY, float VecZ, float VecW);
		~Vec4();

		Vega::Vec4 Scale(float Scalar);
		Vega::Vec4 Add(const Vega::Vec4& Other);
		Vega::Vec4 Subtract(const Vega::Vec4& Other);

		void Scale_M(float Scalar);
		void Add_M(const Vega::Vec4& Other);
		void Subtract_M(const Vega::Vec4& Other);

		float Magnitude();
		float Dot(const Vega::Vec4& Other);
		float MagnitudeSquared();

		float& operator[](int Index);

		friend VEGA_API Vega::Vec4 operator*(const Vega::Vec4& Vec, float Scalar);
		friend VEGA_API Vega::Vec4 operator*(float Scalar, const Vega::Vec4& Vec);
		friend VEGA_API Vega::Vec4 operator+(const Vega::Vec4& Left, const Vega::Vec4& Right);
		friend VEGA_API Vega::Vec4 operator-(const Vega::Vec4& Left, const Vega::Vec4& Right);
		friend VEGA_API Vega::Vec4 operator*=(Vega::Vec4& Left, float Scalar);
		friend VEGA_API Vega::Vec4 operator+=(Vega::Vec4& Left, const Vega::Vec4& Right);
		friend VEGA_API Vega::Vec4 operator-=(Vega::Vec4& Left, const Vega::Vec4& Right);
		friend VEGA_API std::ostream& operator<<(std::ostream& ost, const Vega::Vec4& Vec);

	private:

		float X;
		float Y;
		float Z;
		float W;

	};

	class VEGA_API Vec3
	{
	public:
		Vec3();
		Vec3(const Vega::Vec3& Copy);
		Vec3(float VecX, float VecY, float VecZ);
		~Vec3();

		Vega::Vec3 Scale(float Scalar);
		Vega::Vec3 Add(const Vega::Vec3& Other);
		Vega::Vec3 Subtract(const Vega::Vec3& Other);

		void Scale_M(float Scalar);
		void Add_M(const Vega::Vec3& Other);
		void Subtract_M(const Vega::Vec3& Other);

		float Magnitude();
		float Dot(const Vega::Vec3& Other);
		float MagnitudeSquared();

		float& operator[](int Index);

		friend VEGA_API Vega::Vec3 operator*(const Vega::Vec3& Vec, float Scalar);
		friend VEGA_API Vega::Vec3 operator*(float Scalar, const Vega::Vec3& Vec);
		friend VEGA_API Vega::Vec3 operator+(const Vega::Vec3& Left, const Vega::Vec3& Right);
		friend VEGA_API Vega::Vec3 operator-(const Vega::Vec3& Left, const Vega::Vec3& Right);
		friend VEGA_API Vega::Vec3 operator*=(Vega::Vec3& Left, float Scalar);
		friend VEGA_API Vega::Vec3 operator+=(Vega::Vec3& Left, const Vega::Vec3& Right);
		friend VEGA_API Vega::Vec3 operator-=(Vega::Vec3& Left, const Vega::Vec3& Right);
		friend VEGA_API std::ostream& operator<<(std::ostream& ost, const Vega::Vec3& Vec);

	private:

		float X;
		float Y;
		float Z;

	};

	class VEGA_API Vec2
	{
	public:
		Vec2();
		Vec2(const Vega::Vec2& Copy);
		Vec2(float VecX, float VecY);
		~Vec2();

		Vega::Vec2 Scale(float Scalar);
		Vega::Vec2 Add(const Vega::Vec2& Other);
		Vega::Vec2 Subtract(const Vega::Vec2& Other);

		void Scale_M(float Scalar);
		void Add_M(const Vega::Vec2& Other);
		void Subtract_M(const Vega::Vec2& Other);

		float Magnitude();
		float Dot(const Vega::Vec2& Other);
		float MagnitudeSquared();

		float& operator[](int Index);

		friend VEGA_API Vega::Vec2 operator*(const Vega::Vec2& Vec, float Scalar);
		friend VEGA_API Vega::Vec2 operator*(float Scalar, const Vega::Vec2& Vec);
		friend VEGA_API Vega::Vec2 operator+(const Vega::Vec2& Left, const Vega::Vec2& Right);
		friend VEGA_API Vega::Vec2 operator-(const Vega::Vec2& Left, const Vega::Vec2& Right);
		friend VEGA_API Vega::Vec2 operator*=(Vega::Vec2& Left, float Scalar);
		friend VEGA_API Vega::Vec2 operator+=(Vega::Vec2& Left, const Vega::Vec2& Right);
		friend VEGA_API Vega::Vec2 operator-=(Vega::Vec2& Left, const Vega::Vec2& Right);
		friend VEGA_API std::ostream& operator<<(std::ostream& ost, const Vega::Vec2& Vec);

	private:

		float X;
		float Y;

	};
}
