#pragma once

#include "../Core.h"
#include "Vector.h"
#include <iostream>

namespace Vega
{
	class VEGA_API Mat4
	{
	public:
		Mat4();
		~Mat4();

		void Zero();
		void Identity();
		void Translation(float DX, float DY, float DZ);
		void RotationX(float Theta);
		void RotationY(float Theta);
		void RotationZ(float Theta);
		void RotationXY(float ThetaX, float ThetaY);
		void RotationYX(float ThetaX, float ThetaY);
		void RotationXZ(float ThetaX, float ThetaZ);
		void RotationZX(float ThetaX, float ThetaZ);
		void RotationYZ(float ThetaY, float ThetaZ);
		void RotationZY(float ThetaY, float ThetaZ);
		void RotationXYZ(float ThetaX, float ThetaY, float ThetaZ);
		void RotationXZY(float ThetaX, float ThetaY, float ThetaZ);
		void RotationYXZ(float ThetaX, float ThetaY, float ThetaZ);
		void RotationYZX(float ThetaX, float ThetaY, float ThetaZ);
		void RotationZXY(float ThetaX, float ThetaY, float ThetaZ);
		void RotationZYX(float ThetaX, float ThetaY, float ThetaZ);
		void Scale(float X, float Y, float Z, float W);

		void Ortho(float Left, float Right, float Top, float Bottom, float Near, float Far);

		float* AsArray();

		float GetElement(uint32 Row, uint32 Column) const;
		void SetElement(uint32 Row, uint32 Column, float Value);
		void Set(const Vega::Mat4& Other);
		void Set(const float Dat[4][4]);

		Vega::Mat4 Add(const Vega::Mat4& Other) const;
		Vega::Mat4 Subtract(const Vega::Mat4& Other) const;
		Vega::Mat4 Multiply(const Vega::Mat4& Other) const;
		Vega::Mat4 Scale(float Scalar) const;

		void Add_M(const Vega::Mat4& Other);
		void Subtract_M(const Vega::Mat4& Other);
		void Multiply_M(const Vega::Mat4& Other);
		void Scale_M(float Scalar);

		friend VEGA_API Vega::Mat4 operator+(const Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator-(const Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator*(const Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator*(const Vega::Mat4& Left, float Scalar);
		friend VEGA_API Vega::Mat4 operator*(float Scalar, const Vega::Mat4& Left);
		friend VEGA_API Vega::Vec4 operator*(const Vega::Mat4& Mat, Vega::Vec4& Vec);
		friend VEGA_API std::ostream& operator<<(std::ostream& ost, const Vega::Mat4& Mat);

		friend VEGA_API Vega::Mat4 operator+=(Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator-=(Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator*=(Vega::Mat4& Left, const Vega::Mat4& Right);
		friend VEGA_API Vega::Mat4 operator*=(Vega::Mat4& Left, float Scalar);

	private:

		void MakeRotX(float Mat[4][4], float Theta);
		void MakeRotY(float Mat[4][4], float Theta);
		void MakeRotZ(float Mat[4][4], float Theta);
		void MultMat(float First[4][4], float Second[4][4], float Dst[4][4]);

		float Data[16];

	};

	class VEGA_API Mat3
	{
	public:
		Mat3();
		~Mat3();

		void Zero();
		void Identity();
		void Translation(float DX, float DY);
		void RotationX(float Theta);
		void RotationY(float Theta);
		void RotationZ(float Theta);
		void RotationXY(float ThetaX, float ThetaY);
		void RotationYX(float ThetaX, float ThetaY);
		void RotationXZ(float ThetaX, float ThetaZ);
		void RotationZX(float ThetaX, float ThetaZ);
		void RotationYZ(float ThetaY, float ThetaZ);
		void RotationZY(float ThetaY, float ThetaZ);
		void RotationXYZ(float ThetaX, float ThetaY, float ThetaZ);
		void RotationXZY(float ThetaX, float ThetaY, float ThetaZ);
		void RotationYXZ(float ThetaX, float ThetaY, float ThetaZ);
		void RotationYZX(float ThetaX, float ThetaY, float ThetaZ);
		void RotationZXY(float ThetaX, float ThetaY, float ThetaZ);
		void RotationZYX(float ThetaX, float ThetaY, float ThetaZ);
		void Scale(float DX, float DY, float DZ);

		float* AsArray();

		float GetElement(uint32 Row, uint32 Column) const;
		void SetElement(uint32 Row, uint32 Column, float Value);
		void Set(const Vega::Mat3& Other);
		void Set(const float Dat[3][3]);

		Vega::Mat3 Add(const Vega::Mat3& Other) const;
		Vega::Mat3 Subtract(const Vega::Mat3& Other) const;
		Vega::Mat3 Multiply(const Vega::Mat3& Other) const;
		Vega::Mat3 Scale(float Scalar) const;

		void Add_M(const Vega::Mat3& Other);
		void Subtract_M(const Vega::Mat3& Other);
		void Multiply_M(const Vega::Mat3& Other);
		void Scale_M(float Scalar);

		friend VEGA_API Vega::Mat3 operator+(const Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator-(const Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator*(const Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator*(const Vega::Mat3& Left, float Scalar);
		friend VEGA_API Vega::Mat3 operator*(float Scalar, const Vega::Mat3& Left);
		friend VEGA_API Vega::Vec3 operator*(const Vega::Mat3 Mat, Vega::Vec3& Vec);
		friend VEGA_API std::ostream& operator<<(std::ostream& ost, const Vega::Mat3& Mat);


		friend VEGA_API Vega::Mat3 operator+=(Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator-=(Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator*=(Vega::Mat3& Left, const Vega::Mat3& Right);
		friend VEGA_API Vega::Mat3 operator*=(Vega::Mat3& Left, float Scalar);

	private:

		void MakeRotX(float Mat[3][3], float Theta);
		void MakeRotY(float Mat[3][3], float Theta);
		void MakeRotZ(float Mat[3][3], float Theta);
		void MultMat(float First[3][3], float Second[3][3], float Dst[3][3]);

		float Data[9];

	};
}
