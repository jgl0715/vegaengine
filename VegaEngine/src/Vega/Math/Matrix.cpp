#include "Matrix.h"
#include <math.h>
#include <iomanip>

namespace Vega
{
	Mat3::Mat3()
	{
		Zero();
	}

	Mat3::~Mat3()
	{

	}

	void Mat3::Zero()
	{
		uint32 i;
		uint32 j;

		for (i = 0; i < 3; i++)
		{
			for (j = 0; j < 3; j++)
			{
				Data[i*3+j] = 0.0f;
			}
		}
	}

	void Mat3::Identity()
	{
		uint32 i;
		uint32 j;

		for (i = 0; i < 3; i++)
		{
			for (j = 0; j < 3; j++)
			{
				if (i == j)
					Data[i*3+j] = 1.0f;
				else
					Data[i * 3 + j] = 0.0f;
			}
		}
	}

	void Mat3::Translation(float DX, float DY)
	{
		float Translation[3][3];

		Translation[0][0] = 1.0f;
		Translation[0][1] = 0.0f;
		Translation[0][2] = DX;

		Translation[1][0] = 0.0f;
		Translation[1][1] = 1.0f;
		Translation[1][2] = DY;

		Translation[2][0] = 0.0f;
		Translation[2][1] = 0.0f;
		Translation[2][2] = 1.0;

		Set(Translation);
	}

	void Mat3::RotationX(float Theta)
	{
		float RotX[3][3];
		MakeRotX(RotX, Theta);
		Set(RotX);
	}

	void Mat3::RotationY(float Theta)
	{
		float RotY[3][3];
		MakeRotY(RotY, Theta);
		Set(RotY);
	}

	void Mat3::RotationZ(float Theta)
	{
		float RotZ[3][3];
		MakeRotZ(RotZ, Theta);
		Set(RotZ);
	}

	void Mat3::RotationXY(float ThetaX, float ThetaY)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotXY[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MultMat(RotX, RotY, RotXY);

		Set(RotXY);
	}

	void Mat3::RotationYX(float ThetaX, float ThetaY)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotYZ[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MultMat(RotY, RotX, RotYZ);

		Set(RotYZ);
	}

	void Mat3::RotationXZ(float ThetaX, float ThetaZ)
	{
		float RotX[3][3];
		float RotZ[3][3];
		float RotXZ[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotX, RotZ, RotXZ);

		Set(RotXZ);
	}

	void Mat3::RotationZX(float ThetaX, float ThetaZ)
	{
		float RotX[3][3];
		float RotZ[3][3];
		float RotZX[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotZ, RotX, RotZX);

		Set(RotZX);
	}

	void Mat3::RotationYZ(float ThetaY, float ThetaZ)
	{
		float RotY[3][3];
		float RotZ[3][3];
		float RotYZ[3][3];

		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotY, RotZ, RotYZ);

		Set(RotYZ);
	}

	void Mat3::RotationZY(float ThetaY, float ThetaZ)
	{
		float RotY[3][3];
		float RotZ[3][3];
		float RotZY[3][3];

		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotZ, RotY, RotZY);

		Set(RotZY);
	}

	void Mat3::RotationXYZ(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotXY[3][3];
		float RotXYZ[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotX, RotY, RotXY);
		MultMat(RotXY, RotZ, RotXYZ);

		Set(RotXYZ);
	}

	void Mat3::RotationXZY(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotXZ[3][3];
		float RotXZY[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotX, RotZ, RotXZ);
		MultMat(RotXZ, RotY, RotXZY);

		Set(RotXZY);
	}

	void Mat3::RotationYXZ(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotYX[3][3];
		float RotYXZ[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotY, RotX, RotYX);
		MultMat(RotYX, RotZ, RotYXZ);

		Set(RotYXZ);
	}

	void Mat3::RotationYZX(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotYZ[3][3];
		float RotYZX[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotY, RotZ, RotYZ);
		MultMat(RotYZ, RotX, RotYZX);

		Set(RotYZX);
	}

	void Mat3::RotationZXY(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotZX[3][3];
		float RotZXY[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotZ, RotX, RotZX);
		MultMat(RotZX, RotY, RotZXY);

		Set(RotZXY);
	}

	void Mat3::RotationZYX(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[3][3];
		float RotY[3][3];
		float RotZ[3][3];
		float RotZY[3][3];
		float RotZYX[3][3];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotZ, RotY, RotZY);
		MultMat(RotZY, RotX, RotZYX);

		Set(RotZYX);
	}

	float* Mat3::AsArray()
	{
		return (float*)(this->Data);
	}

	void Mat3::Scale(float X, float Y, float Z)
	{
		float Scale[3][3];

		Scale[0][0] = X;
		Scale[0][1] = 0.0f;
		Scale[0][2] = 0.0f;

		Scale[1][0] = 0.0f;
		Scale[1][1] = Y;
		Scale[1][2] = 0.0f;

		Scale[2][0] = 0.0f;
		Scale[2][1] = 0.0f;
		Scale[2][2] = Z;

		Set(Scale);
	}

	float Mat3::GetElement(uint32 Row, uint32 Column) const
	{
		return Data[Row*3+Column];
	}

	void Mat3::SetElement(uint32 Row, uint32 Column, float Value)
	{
		Data[Row*3+Column] = Value;
	}

	void Mat3::Set(const Vega::Mat3& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Data[Row*3+Column] = Other.Data[Row * 3 + Column];
			}
		}
	}

	void Mat3::Set(const float Dat[3][3])
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Data[Row*3+Column] = Dat[Row][Column];
			}
		}
	}

	Vega::Mat3 Mat3::Add(const Vega::Mat3& Other) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat3 Result;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Result.Data[Row * 3 + Column] = Data[Row * 3 + Column] + Other.Data[Row * 3 + Column];
			}
		}

		return Result;
	}

	Vega::Mat3 Mat3::Subtract(const Vega::Mat3& Other) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat3 Result;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Result.Data[Row * 3 + Column] = Data[Row * 3 + Column] - Other.Data[Row * 3 + Column];
			}
		}

		return Result;
	}

	Vega::Mat3 Mat3::Multiply(const Vega::Mat3& Other) const
	{
		uint32 Row;
		uint32 Column;
		uint32 J;
		Vega::Mat3 Result;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 3; J++)
					Dot += Data[Row*3+J] * Other.Data[J*3+Column];

				Result.Data[Row * 3 + Column] = Dot;
			}
		}

		return Result;
	}

	Vega::Mat3 Mat3::Scale(float Scalar) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat3 Result;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Result.Data[Row * 3 + Column] = Data[Row * 3 + Column] * Scalar;
			}
		}

		return Result;
	}

	void Mat3::Add_M(const Vega::Mat3& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Data[Row * 3 + Column] += Other.Data[Row * 3 + Column];
			}
		}
	}

	void Mat3::Subtract_M(const Vega::Mat3& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Data[Row * 3 + Column] -= Other.Data[Row * 3 + Column];
			}
		}
	}

	void Mat3::Multiply_M(const Vega::Mat3& Other)
	{
		uint32 Row;
		uint32 Column;
		uint32 J;
		Vega::Mat3 Result;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 3; J++)
					Dot += Data[Row*3+J] * Other.Data[J*3+Column];

				Result.Data[Row * 3 + Column] = Dot;
			}
		}

		Set(Result);
	}

	void Mat3::Scale_M(float Scalar)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				Data[Row * 3 + Column] *= Scalar;
			}
		}
	}

	Vega::Mat3 operator+(const Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		return Left.Add(Right);
	}

	Vega::Mat3 operator-(const Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		return Left.Subtract(Right);
	}

	Vega::Mat3 operator*(const Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		return Left.Multiply(Right);
	}

	Vega::Mat3 operator*(const Vega::Mat3& Left, float Scalar)
	{
		return Left.Scale(Scalar);
	}

	Vega::Mat3 operator*(const float Scalar, Vega::Mat3& Right)
	{
		return Right.Scale(Scalar);
	}

	Vega::Vec3 operator*(const Vega::Mat3 Mat, Vega::Vec3& Vec)
	{
		float X = Mat.GetElement(0, 0) * Vec[0] + Mat.GetElement(0, 1) * Vec[1] + Mat.GetElement(0, 2) * Vec[2];
		float Y = Mat.GetElement(1, 0) * Vec[0] + Mat.GetElement(1, 1) * Vec[1] + Mat.GetElement(1, 2) * Vec[2];
		float Z = Mat.GetElement(2, 0) * Vec[0] + Mat.GetElement(2, 1) * Vec[1] + Mat.GetElement(2, 2) * Vec[2];

		return Vega::Vec3(X, Y, Z);
	}

	std::ostream& operator<<(std::ostream& ost, const Vega::Mat3& Mat)
	{
		std::cout << std::fixed << std::setprecision(2);

		std::cout << Mat.Data[0] << " " << Mat.Data[1] << " " << Mat.Data[2] << std::endl;
		std::cout << Mat.Data[3] << " " << Mat.Data[4] << " " << Mat.Data[5] << std::endl;
		std::cout << Mat.Data[6] << " " << Mat.Data[7] << " " << Mat.Data[8];
		
		return ost;
	}

	Vega::Mat3 operator+=(Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		Left.Add_M(Right);

		return Left;
	}

	Vega::Mat3 operator-=(Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		Left.Subtract_M(Right);

		return Left;
	}

	Vega::Mat3 operator*=(Vega::Mat3& Left, const Vega::Mat3& Right)
	{
		Left.Multiply_M(Right);

		return Left;
	}

	Vega::Mat3 operator*=(Vega::Mat3& Left, float Scalar)
	{
		Left.Scale_M(Scalar);

		return Left;
	}

	void Mat3::MakeRotX(float Mat[3][3], float Theta)
	{

		float Sine = sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = 1.0f;
		Mat[0][1] = 0.0f;
		Mat[0][2] = 0.0f;

		Mat[1][0] = 0.0f;
		Mat[1][1] = Cosine;
		Mat[1][2] = -Sine;

		Mat[2][0] = 0.0f;
		Mat[2][1] = Sine;
		Mat[2][2] = Cosine;

	}
	
	void Mat3::MakeRotY(float Mat[3][3], float Theta)
	{

		float Sine = sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = Cosine;
		Mat[0][1] = 0.0f;
		Mat[0][2] = Sine;

		Mat[1][0] = 0.0f;
		Mat[1][1] = 1.0f;
		Mat[1][2] = 0.0f;

		Mat[2][0] = -Sine;
		Mat[2][1] = 0.0f;
		Mat[2][2] = Cosine;
	}
	
	void Mat3::MakeRotZ(float Mat[3][3], float Theta)
	{

		float Sine =   sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = Cosine;
		Mat[0][1] = -Sine;
		Mat[0][2] = 0.0f;

		Mat[1][0] = Sine;
		Mat[1][1] = Cosine;
		Mat[1][2] = 0.0f;

		Mat[2][0] = 0.0f;
		Mat[2][1] = 0.0f;
		Mat[2][2] = 1.0f;
	}

	void Mat3::MultMat(float First[3][3], float Second[3][3], float Dst[3][3])
	{
		uint32 Row;
		uint32 Column;
		uint32 J;

		for (Row = 0; Row < 3; Row++)
		{
			for (Column = 0; Column < 3; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 3; J++)
					Dot += First[Row][J] * Second[J][Column];

				Dst[Row][Column] = Dot;
			}
		}
	}

	// MATRIX4 DEFINITIONS
	Mat4::Mat4()
	{
		Zero();
	}

	Mat4::~Mat4()
	{

	}

	void Mat4::Zero()
	{
		uint32 i;
		uint32 j;

		for (i = 0; i < 4; i++)
		{
			for (j = 0; j < 4; j++)
			{
				Data[i * 4 + j] = 0.0f;
			}
		}
	}

	void Mat4::Identity()
	{
		uint32 i;
		uint32 j;

		for (i = 0; i < 4; i++)
		{
			for (j = 0; j < 4; j++)
			{
				if (i == j)
					Data[i * 4 + j] = 1.0f;
				else
					Data[i * 4 + j] = 0.0f;
			}
		}
	}

	void Mat4::Translation(float DX, float DY, float DZ)
	{
		float Translation[4][4];

		Translation[0][0] = 1.0f;
		Translation[0][1] = 0.0f;
		Translation[0][2] = 0.0f;
		Translation[0][3] = DX;

		Translation[1][0] = 0.0f;
		Translation[1][1] = 1.0f;
		Translation[1][2] = 0.0f;
		Translation[1][3] = DY;

		Translation[2][0] = 0.0f;
		Translation[2][1] = 0.0f;
		Translation[2][2] = 1.0;
		Translation[2][3] = DZ;

		Translation[3][0] = 0.0f;
		Translation[3][1] = 0.0f;
		Translation[3][2] = 0.0f;
		Translation[3][3] = 1.0;

		Set(Translation);
	}

	void Mat4::RotationX(float Theta)
	{
		float RotX[4][4];
		MakeRotX(RotX, Theta);
		Set(RotX);
	}

	void Mat4::RotationY(float Theta)
	{
		float RotY[4][4];
		MakeRotY(RotY, Theta);
		Set(RotY);
	}

	void Mat4::RotationZ(float Theta)
	{
		float RotZ[4][4];
		MakeRotZ(RotZ, Theta);
		Set(RotZ);
	}

	void Mat4::RotationXY(float ThetaX, float ThetaY)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotXY[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MultMat(RotX, RotY, RotXY);

		Set(RotXY);
	}

	void Mat4::RotationYX(float ThetaX, float ThetaY)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotYZ[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MultMat(RotY, RotX, RotYZ);

		Set(RotYZ);
	}

	void Mat4::RotationXZ(float ThetaX, float ThetaZ)
	{
		float RotX[4][4];
		float RotZ[4][4];
		float RotXZ[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotX, RotZ, RotXZ);

		Set(RotXZ);
	}

	void Mat4::RotationZX(float ThetaX, float ThetaZ)
	{
		float RotX[4][4];
		float RotZ[4][4];
		float RotZX[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotZ, RotX, RotZX);

		Set(RotZX);
	}

	void Mat4::RotationYZ(float ThetaY, float ThetaZ)
	{
		float RotY[4][4];
		float RotZ[4][4];
		float RotYZ[4][4];

		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotY, RotZ, RotYZ);

		Set(RotYZ);
	}

	void Mat4::RotationZY(float ThetaY, float ThetaZ)
	{
		float RotY[4][4];
		float RotZ[4][4];
		float RotZY[4][4];

		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);
		MultMat(RotZ, RotY, RotZY);

		Set(RotZY);
	}

	void Mat4::RotationXYZ(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotXY[4][4];
		float RotXYZ[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotX, RotY, RotXY);
		MultMat(RotXY, RotZ, RotXYZ);

		Set(RotXYZ);
	}

	void Mat4::RotationXZY(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotXZ[4][4];
		float RotXZY[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotX, RotZ, RotXZ);
		MultMat(RotXZ, RotY, RotXZY);

		Set(RotXZY);
	}

	void Mat4::RotationYXZ(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotYX[4][4];
		float RotYXZ[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotY, RotX, RotYX);
		MultMat(RotYX, RotZ, RotYXZ);

		Set(RotYXZ);
	}

	void Mat4::RotationYZX(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotYZ[4][4];
		float RotYZX[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotY, RotZ, RotYZ);
		MultMat(RotYZ, RotX, RotYZX);

		Set(RotYZX);
	}

	void Mat4::RotationZXY(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotZX[4][4];
		float RotZXY[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotZ, RotX, RotZX);
		MultMat(RotZX, RotY, RotZXY);

		Set(RotZXY);
	}

	void Mat4::RotationZYX(float ThetaX, float ThetaY, float ThetaZ)
	{
		float RotX[4][4];
		float RotY[4][4];
		float RotZ[4][4];
		float RotZY[4][4];
		float RotZYX[4][4];

		MakeRotX(RotX, ThetaX);
		MakeRotY(RotY, ThetaY);
		MakeRotZ(RotZ, ThetaZ);

		MultMat(RotZ, RotY, RotZY);
		MultMat(RotZY, RotX, RotZYX);

		Set(RotZYX);
	}

	float* Mat4::AsArray()
	{
		return (float*)(this->Data);
	}

	void Mat4::Scale(float X, float Y, float Z, float W)
	{
		float Scale[4][4];

		Scale[0][0] = X;
		Scale[0][1] = 0.0f;
		Scale[0][2] = 0.0f;
		Scale[0][3] = 0.0f;

		Scale[1][0] = 0.0f;
		Scale[1][1] = Y;
		Scale[1][2] = 0.0f;
		Scale[1][3] = 0.0f;

		Scale[2][0] = 0.0f;
		Scale[2][1] = 0.0f;
		Scale[2][2] = Z;
		Scale[2][3] = 0.0f;

		Scale[3][0] = 0.0f;
		Scale[3][1] = 0.0f;
		Scale[3][2] = 0.0f;
		Scale[3][3] = W;

		Set(Scale);
	}

	void Mat4::Ortho(float Left, float Right, float Bottom, float Top, float Near, float Far)
	{
		float Ortho[4][4];

		float XRange = Right - Left;
		float YRange = Top - Bottom;
		float ZRange = Far - Near;

		Ortho[0][0] = 2.0f / XRange;
		Ortho[0][1] = 0.0f;
		Ortho[0][2] = 0.0f;
		Ortho[0][3] = -(Right + Left) / XRange;

		Ortho[1][0] = 0.0f;
		Ortho[1][1] = 2.0f / (YRange);
		Ortho[1][2] = 0.0f;
		Ortho[1][3] = -(Top + Bottom) / YRange;

		Ortho[2][0] = 0.0f;
		Ortho[2][1] = 0.0f;
		Ortho[2][2] = -2.0f / ZRange;
		Ortho[2][3] = -(Far + Near) / ZRange;

		Ortho[3][0] = 0.0f;
		Ortho[3][1] = 0.0f;
		Ortho[3][2] = 0.0f;
		Ortho[3][3] = 1.0f;

		Set(Ortho);
	}

	float Mat4::GetElement(uint32 Row, uint32 Column) const
	{
		return Data[Row * 4 + Column];
	}

	void Mat4::SetElement(uint32 Row, uint32 Column, float Value)
	{
		Data[Row * 4 + Column] = Value;
	}

	void Mat4::Set(const Vega::Mat4& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Data[Row * 4 + Column] = Other.Data[Row * 4 + Column];
			}
		}
	}

	void Mat4::Set(const float Dat[4][4])
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Data[Row * 4 + Column] = Dat[Row][Column];
			}
		}
	}

	Vega::Mat4 Mat4::Add(const Vega::Mat4& Other) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat4 Result;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Result.Data[Row * 4 + Column] = Data[Row * 4 + Column] + Other.Data[Row * 4 + Column];
			}
		}

		return Result;
	}

	Vega::Mat4 Mat4::Subtract(const Vega::Mat4& Other) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat4 Result;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Result.Data[Row * 4 + Column] = Data[Row * 4 + Column] - Other.Data[Row * 4 + Column];
			}
		}

		return Result;
	}

	Vega::Mat4 Mat4::Multiply(const Vega::Mat4& Other) const
	{
		uint32 Row;
		uint32 Column;
		uint32 J;
		Vega::Mat4 Result;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 4; J++)
					Dot += Data[Row * 4 + J] * Other.Data[J * 4 + Column];

				Result.Data[Row * 4 + Column] = Dot;
			}
		}

		return Result;
	}

	Vega::Mat4 Mat4::Scale(float Scalar) const
	{
		uint32 Row;
		uint32 Column;
		Vega::Mat4 Result;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Result.Data[Row * 4 + Column] = Data[Row * 4 + Column] * Scalar;
			}
		}

		return Result;
	}

	void Mat4::Add_M(const Vega::Mat4& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Data[Row * 4 + Column] += Other.Data[Row * 4 + Column];
			}
		}
	}

	void Mat4::Subtract_M(const Vega::Mat4& Other)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Data[Row * 4 + Column] -= Other.Data[Row * 4 + Column];
			}
		}
	}

	void Mat4::Multiply_M(const Vega::Mat4& Other)
	{
		uint32 Row;
		uint32 Column;
		uint32 J;
		Vega::Mat4 Result;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 4; J++)
					Dot += Data[Row * 4 + J] * Other.Data[J * 4 + Column];

				Result.Data[Row * 4 + Column] = Dot;
			}
		}

		Set(Result);
	}

	void Mat4::Scale_M(float Scalar)
	{
		uint32 Row;
		uint32 Column;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				Data[Row * 4 + Column] *= Scalar;
			}
		}
	}

	Vega::Mat4 operator+(const Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		return Left.Add(Right);
	}

	Vega::Mat4 operator-(const Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		return Left.Subtract(Right);
	}

	Vega::Mat4 operator*(const Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		return Left.Multiply(Right);
	}

	Vega::Mat4 operator*(const Vega::Mat4& Left, float Scalar)
	{
		return Left.Scale(Scalar);
	}

	Vega::Mat4 operator*(const float Scalar, Vega::Mat4& Right)
	{
		return Right.Scale(Scalar);
	}

	Vega::Vec4 operator*(const Vega::Mat4& Mat, Vega::Vec4& Vec)
	{
		float X = Mat.GetElement(0, 0) * Vec[0] + Mat.GetElement(0, 1) * Vec[1] + Mat.GetElement(0, 2) * Vec[2] + Mat.GetElement(0, 3) * Vec[3];
		float Y = Mat.GetElement(1, 0) * Vec[0] + Mat.GetElement(1, 1) * Vec[1] + Mat.GetElement(1, 2) * Vec[2] + Mat.GetElement(1, 3) * Vec[3];
		float Z = Mat.GetElement(2, 0) * Vec[0] + Mat.GetElement(2, 1) * Vec[1] + Mat.GetElement(2, 2) * Vec[2] + Mat.GetElement(2, 3) * Vec[3];
		float W = Mat.GetElement(3, 0) * Vec[0] + Mat.GetElement(3, 1) * Vec[1] + Mat.GetElement(3, 2) * Vec[2] + Mat.GetElement(3, 3) * Vec[3];

		return Vega::Vec4(X, Y, Z, W);
	}

	std::ostream& operator<<(std::ostream& ost, const Vega::Mat4& Mat)
	{
		std::cout << std::fixed << std::setprecision(2);

		std::cout << Mat.Data[0] << " " << Mat.Data[1] << " " << Mat.Data[2] << Mat.Data[3] << std::endl;
		std::cout << Mat.Data[4] << " " << Mat.Data[5] << " " << Mat.Data[6] << Mat.Data[7] << std::endl;
		std::cout << Mat.Data[8] << " " << Mat.Data[9] << " " << Mat.Data[10] << Mat.Data[11] << std::endl;
		std::cout << Mat.Data[12] << " " << Mat.Data[13] << " " << Mat.Data[14] << Mat.Data[15] << std::endl;

		return ost;
	}

	Vega::Mat4 operator+=(Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		Left.Add_M(Right);

		return Left;
	}

	Vega::Mat4 operator-=(Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		Left.Subtract_M(Right);

		return Left;
	}

	Vega::Mat4 operator*=(Vega::Mat4& Left, const Vega::Mat4& Right)
	{
		Left.Multiply_M(Right);

		return Left;
	}

	Vega::Mat4 operator*=(Vega::Mat4& Left, float Scalar)
	{
		Left.Scale_M(Scalar);

		return Left;
	}

	void Mat4::MakeRotX(float Mat[4][4], float Theta)
	{

		float Sine = sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = 1.0f;
		Mat[0][1] = 0.0f;
		Mat[0][2] = 0.0f;
		Mat[0][3] = 0.0f;

		Mat[1][0] = 0.0f;
		Mat[1][1] = Cosine;
		Mat[1][2] = -Sine;
		Mat[1][3] = 0.0f;

		Mat[2][0] = 0.0f;
		Mat[2][1] = Sine;
		Mat[2][2] = Cosine;
		Mat[2][3] = 0.0f;

		Mat[3][0] = 0.0f;
		Mat[3][1] = 0.0f;
		Mat[3][2] = 0.0f;
		Mat[3][3] = 1.0f;

	}

	void Mat4::MakeRotY(float Mat[4][4], float Theta)
	{

		float Sine = sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = Cosine;
		Mat[0][1] = 0.0f;
		Mat[0][2] = Sine;
		Mat[0][3] = 0.0f;

		Mat[1][0] = 0.0f;
		Mat[1][1] = 1.0f;
		Mat[1][2] = 0.0f;
		Mat[1][3] = 0.0f;

		Mat[2][0] = -Sine;
		Mat[2][1] = 0.0f;
		Mat[2][2] = Cosine;
		Mat[2][3] = 0.0f;

		Mat[3][0] = 0.0f;
		Mat[3][1] = 0.0f;
		Mat[3][2] = 0.0f;
		Mat[3][3] = 1.0f;

	}

	void Mat4::MakeRotZ(float Mat[4][4], float Theta)
	{

		float Sine = sin(Theta);
		float Cosine = cos(Theta);

		Mat[0][0] = Cosine;
		Mat[0][1] = -Sine;
		Mat[0][2] = 0.0f;
		Mat[0][3] = 0.0f;

		Mat[1][0] = Sine;
		Mat[1][1] = Cosine;
		Mat[1][2] = 0.0f;
		Mat[1][3] = 0.0f;

		Mat[2][0] = 0.0f;
		Mat[2][1] = 0.0f;
		Mat[2][2] = 1.0f;
		Mat[2][3] = 0.0f;

		Mat[3][0] = 0.0f;
		Mat[3][1] = 0.0f;
		Mat[3][2] = 0.0f;
		Mat[3][3] = 1.0f;

	}

	void Mat4::MultMat(float First[4][4], float Second[4][4], float Dst[4][4])
	{
		uint32 Row;
		uint32 Column;
		uint32 J;

		for (Row = 0; Row < 4; Row++)
		{
			for (Column = 0; Column < 4; Column++)
			{
				float Dot = 0.0f;

				for (J = 0; J < 4; J++)
					Dot += First[Row][J] * Second[J][Column];

				Dst[Row][Column] = Dot;
			}
		}
	}


}