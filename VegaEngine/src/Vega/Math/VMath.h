#pragma once

#include "Matrix.h"
#include "Vector.h"

#define PI 3.14159265f
#define D_TO_R PI / 180.0f
