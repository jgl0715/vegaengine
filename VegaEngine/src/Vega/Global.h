#pragma once

#include "Core.h"

namespace Vega
{
	class DesktopApplication;
	class ResourceManager;
	class Shader;

#ifdef VG_PLATFORM_WINDOWS

	extern VEGA_API Vega::DesktopApplication* DesktopApp;

#endif

	namespace Graphics
	{

		enum class VEGA_API RAPI : uint32
		{
			OPENGL4,
			DX3D,
			VULKAN,
			SOFTWARE
		};

		extern VEGA_API Vega::Graphics::RAPI RenderingAPI;

		VEGA_API int32 GetWidth();
		VEGA_API int32 GetHeight();

		VEGA_API Vega::Shader* NewShader();
	}

	namespace App
	{
		VEGA_API int32 GetFps();
		VEGA_API int32 GetUps();
	}

	namespace Res
	{
		extern VEGA_API Vega::ResourceManager* Man;
	}

}