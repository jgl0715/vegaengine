#include "BinaryFile.h"

namespace Vega
{

	BinaryFile::BinaryFile()
	{
		FileStream = new std::ifstream;
	}


	BinaryFile::~BinaryFile()
	{

	}

	void BinaryFile::SkipBytes(uint32 Count)
	{
		uint32 Index;
		uint8 TrashByte;

		for (Index = 0; Index < Count; Index++)
			ReadUInt8(&TrashByte);
	}

	void BinaryFile::ReadString(char* Destination, uint32 Count)
	{
		FileStream->read(Destination, Count);
	}

	void BinaryFile::ReadBytes(uint8* Destination, uint32 Count)
	{
		FileStream->read(reinterpret_cast<char*>(Destination), Count);
	}
	
	void BinaryFile::ReadUInt32(uint32* Destination)
	{
		FileStream->read(reinterpret_cast<char*>(Destination), sizeof(uint32));
	}
	
	void BinaryFile::ReadUInt16(uint16* Destination)
	{
		FileStream->read(reinterpret_cast<char*>(Destination), sizeof(uint16));
	}
	
	void BinaryFile::ReadUInt8(uint8* Destination)
	{
		FileStream->read(reinterpret_cast<char*>(Destination), sizeof(uint8));
	}
	
	void BinaryFile::ReadFloat(float* Destination)
	{

	}

	void BinaryFile::Open(const std::string& Path)
	{
		FileStream->open(Path, std::ios::in | std::ios::binary);
	}
	
	void BinaryFile::Close()
	{
		FileStream->close();
	}

	bool BinaryFile::IsOpen()
	{
		return FileStream->is_open();
	}

	void BinaryFile::SeekBeg(uint32 Location)
	{
		FileStream->seekg(Location, std::ios_base::beg);
	}

	void BinaryFile::SeekCur(uint32 Location)
	{
		FileStream->seekg(Location, std::ios_base::cur);
	}
	
	void BinaryFile::SeekEnd(uint32 Location)
	{
		FileStream->seekg(Location, std::ios_base::end);
	}

}
