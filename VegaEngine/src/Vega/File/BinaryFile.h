#pragma once

#include "../Core.h"

#include <string>
#include <fstream>

namespace Vega
{
	class VEGA_API BinaryFile
	{
	public:
		BinaryFile();
		~BinaryFile();

		void Open(const std::string& Path);
		void Close();
		bool IsOpen();
		void SeekBeg(uint32 Location);
		void SeekCur(uint32 Location);
		void SeekEnd(uint32 Location);

		void SkipBytes(uint32 Count);
		void ReadString(char* Destination, uint32 Count);
		void ReadBytes(uint8* Destination, uint32 Count);
		void ReadUInt32(uint32* Destination);
		void ReadUInt16(uint16* Destination);
		void ReadUInt8(uint8* Destination);
		void ReadFloat(float* Destination);

	private:

		std::ifstream* FileStream;
	};

}
