#include "Core/ApplicationListener.h"
#include "Core/Desktop/DesktopApplication.h"
#include "Core/Log.h"
#include "Global.h"
#include <iostream>

#define WIDTH 16*60
#define HEIGHT 9*60

#define ERR_NONE 0
#define ERR_GLFW_INIT_FAIL -1
#define ERR_GLFW_WIND_FAIL -2

/*
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	std::cout << key << " was pressed." << std::endl;
}*/

#ifdef VG_PLATFORM_WINDOWS

int main(int argc, char* argv[])
{

	Vega::Graphics::RenderingAPI = Vega::Graphics::RAPI::OPENGL4;
	Vega::Res::Man = new Vega::ResourceManager;

	auto App = Vega::CreateApplicationListener();

	Vega::DesktopApp = new Vega::DesktopApplication(App);
	
	try
	{
		Vega::DesktopApp->Launch();
	}
	catch (const char* exp)
	{
		VG_LOG(LOG_ERROR, "BEGIN ERROR TRACE");
		VG_LOG(LOG_ERROR, exp);
		VG_LOG(LOG_ERROR, "END ERROR TRACE");
		system("pause");
	}

}

#endif