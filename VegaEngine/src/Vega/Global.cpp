#include "Global.h"
#include "Core/Desktop/DesktopApplication.h"
#include "Core/Desktop/Window.h"
#include "Resource/ResourceManager.h"

// Graphics
#include "Rendering/Shader.h"
#include "Rendering/GL/GLTexture.h"
#include "Rendering/GL/GLShader.h"

namespace Vega
{

	Vega::DesktopApplication* DesktopApp;

	namespace Graphics
	{
		Vega::Graphics::RAPI RenderingAPI;

#ifdef VG_PLATFORM_WINDOWS
		int32 GetWidth()
		{
			return DesktopApp->GetWindow()->GetWidth();
		}

		int32 GetHeight()
		{
			return DesktopApp->GetWindow()->GetHeight();
		}
#endif

		Vega::Shader* NewShader()
		{
			Vega::Shader* ShaderResult = nullptr;
			if (Vega::Graphics::RenderingAPI == Vega::Graphics::RAPI::OPENGL4)
			{
				ShaderResult = new GLShader;
			}

			return ShaderResult;
		}


	}

	namespace App
	{

#ifdef VG_PLATFORM_WINDOWS

		int32 GetFps()
		{
			return DesktopApp->GetFps();
		}

		int32 GetUps()
		{
			return DesktopApp->GetUps();
		}
#endif

	}

	namespace Res
	{
		Vega::ResourceManager* Man;
	}

}