#pragma once

#include "../Resource/Resource.h"
#include "Image/Bitmap.h"
#include <string>

namespace Vega
{
	class VEGA_API Texture : public Vega::Resource
	{
	public:
		Texture();
		virtual ~Texture();

		virtual void Bind() const = 0;
		virtual void Load(const std::string& Path);
		virtual void Free() = 0;

		virtual void LoadBmp(Vega::Bitmap& Bitmap);

		virtual uint32 GetWidth() const = 0;
		virtual uint32 GetHeight() const = 0;

		virtual bool operator!=(const Texture* Other) const = 0;
		virtual bool operator==(const Texture* Other) const = 0;

	protected:

		uint32 Width;
		uint32 Height;
	};
}
