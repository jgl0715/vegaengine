#pragma once

#include "../Math/Matrix.h"
#include <string>

namespace Vega
{
	class VEGA_API Shader
	{
	public:
		Shader() {}
		virtual ~Shader() {}

		// Utilization
		virtual void UseProgram() = 0;

		// Compile/Linking functions
		virtual void CompileAndLink(const std::string& VSPath, const std::string& FSPath) = 0;

		// Uniform setting functions
		virtual void SetUniformMat3f(const std::string& UniformName, Vega::Mat3& Matrix) = 0;
		virtual void SetUniformMat4f(const std::string& UniformName, Vega::Mat4& Matrix) = 0;

	};
}

