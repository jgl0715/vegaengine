#include "Bitmap.h"

#include  "../../File/BinaryFile.h"
#include <fstream>
#include <string.h>

namespace Vega
{
	Bitmap::Bitmap()
	{

	}


	Bitmap::~Bitmap()
	{

	}

	void Bitmap::Load(const std::string& Path)
	{
		Vega::BinaryFile File;
		File.Open(Path);

		if (File.IsOpen())
		{

			// File header

			BMPFileHeader FileHeader;
			BMPImageHeader ImageHeader;
			uint32 ImageX;
			uint32 ImageY;
			uint32 Padding;
			uint8 ImageAlpha;
			uint8 ImageRed;
			uint8 ImageGreen;
			uint8 ImageBlue;

			File.ReadString(FileHeader.FileType, 2);
			File.ReadUInt32(&FileHeader.FileSize);
			File.ReadBytes(FileHeader.Reserved, 4);
			File.ReadUInt32(&FileHeader.DataOffset);

			File.ReadUInt32(&ImageHeader.HeaderSize);
			File.ReadUInt32(&ImageHeader.ImageWidth);
			File.ReadUInt32(&ImageHeader.ImageHeight);
			File.ReadUInt16(&ImageHeader.BitPlanes);
			File.ReadUInt16(&ImageHeader.BitsPerPixel);
			File.ReadUInt32(&ImageHeader.CompressionType);
			File.ReadUInt32(&ImageHeader.ImageSize);
			File.ReadUInt32(&ImageHeader.ResX);
			File.ReadUInt32(&ImageHeader.ResY);
			File.ReadUInt32(&ImageHeader.ColorMapsUsed);
			File.ReadUInt32(&ImageHeader.SignificantColors);

			if (ImageHeader.BitsPerPixel % 8 != 0)
			{
				throw "Only 8 bits per pixel is supported";
			}

			Channels = ImageHeader.BitsPerPixel / 8;
			Width = ImageHeader.ImageWidth;
			Height = ImageHeader.ImageHeight;
			Padding = ((uint32) ceil(Width * Channels / 4.0f)) * 4 - (Width * Channels);
			Data = new uint8[Width * Height * Channels];

			// Seek to the data position
			File.SeekBeg(FileHeader.DataOffset);

			for (ImageX = 0; ImageX < Width; ImageX++)
			{
				// Bitmaps usually start scanning from bottom to top
				for (ImageY = 0; ImageY < Height; ImageY++)
				{

					// Setup default color values;
					ImageAlpha = 255;
					ImageRed = 0;
					ImageGreen = 0;
					ImageBlue = 0;

					// In CImg:
					// Alpha = Channel 3
					// Blue  = Channel 2
					// Green = Channel 1
					// Red   = Channel 0

					// We're using switch fall through here because it cleanly represents
					// the kind of logic I'm looking for. The switch will fall through to
					// all cases with lower valued selectors than itself. So we can have
					// R, RG, RGB, and RGBA images.

					if (Channels == 1)
					{
						File.ReadUInt8(&ImageRed);
					}
					else if (Channels == 2)
					{
						File.ReadUInt8(&ImageGreen);
						File.ReadUInt8(&ImageRed);
					}
					else if (Channels == 3)
					{
						File.ReadUInt8(&ImageBlue);
						File.ReadUInt8(&ImageGreen);
						File.ReadUInt8(&ImageRed);
					}
					else if (Channels == 4)
					{
						File.ReadUInt8(&ImageBlue);
						File.ReadUInt8(&ImageGreen);
						File.ReadUInt8(&ImageRed);
						File.ReadUInt8(&ImageAlpha);
					}

					// Setup image in an interleaved RGBA format (R1G1B1A1R2G2B2A2...)
					Data[Channels * (ImageX * Height + ImageY) + 0] = ImageRed;
					Data[Channels * (ImageX * Height + ImageY) + 1] = ImageGreen;
					Data[Channels * (ImageX * Height + ImageY) + 2] = ImageBlue;
					Data[Channels * (ImageX * Height + ImageY) + 3] = ImageAlpha;

				}

				File.SeekCur(Padding);
			}

			File.Close();
		}
		else
		{
			throw "Bitmap file failed to open";
		}

	}
	
	void Bitmap::Free()
	{
		delete Data;
	}

	uint32 Bitmap::GetWidth() const
	{
		return Width;
	}
	
	uint32 Bitmap::GetHeight() const
	{
		return Height;
	}
	
	uint32 Bitmap::GetChannels() const
	{
		return Channels;
	}

	uint8* Bitmap::GetData()
	{
		return Data;
	}

}

