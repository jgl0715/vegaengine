#pragma once

#include <string>
#include "../../Core/Log.h"
#include "../../Core.h"

namespace Vega
{

	struct BMPFileHeader
	{
		char FileType[2];
		uint32 FileSize;
		uint8 Reserved[4];
		uint32 DataOffset;
	};

	struct BMPImageHeader
	{
		uint32 HeaderSize;
		uint32 ImageWidth;
		uint32 ImageHeight;
		uint16 BitPlanes;
		uint16 BitsPerPixel;
		uint32 CompressionType;
		uint32 ImageSize;
		uint32 ResX;
		uint32 ResY;
		uint32 ColorMapsUsed;
		uint32 SignificantColors;
	};

	class VEGA_API Bitmap
	{
	public:
		Bitmap();
		~Bitmap();

		void Load(const std::string& ImageFile);
		void Free();

		uint32 GetWidth() const;
		uint32 GetHeight() const;
		uint32 GetChannels() const;
		uint8* GetData();

	private:

		uint32 Width;
		uint32 Height;
		uint32 Channels;
		uint8* Data;

	};
}


