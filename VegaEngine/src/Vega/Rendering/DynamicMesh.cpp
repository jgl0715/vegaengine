#include "DynamicMesh.h"

namespace Vega
{

	DynamicMesh::DynamicMesh()
	{

	}

	DynamicMesh::~DynamicMesh()
	{

	}

	void DynamicMesh::Create(uint32 MaxVertices)
	{
		this->MaxVertices = MaxVertices;
		this->MeshData = new float[MaxVertices * VERTEX_ELEMENTS];
		this->Vertices = 0;

		VAO.Create();
		VAO.SetUsage(GL_DYNAMIC_DRAW);
	}

	void DynamicMesh::Free()
	{
		VAO.Free();

		delete MeshData;
	}

	void DynamicMesh::Render()
	{
		VAO.Render(Vertices);
	}

	void DynamicMesh::AddVertex(Vega::Vertex& Vert)
	{
		if (Vertices >= MaxVertices)
			throw "Dynamic mesh vertex overflow";

		uint32 Offset = VERTEX_ELEMENTS * Vertices;

		MeshData[Offset + 0] = Vert.Position[0];
		MeshData[Offset + 1] = Vert.Position[1];
		MeshData[Offset + 2] = Vert.Position[2];
		MeshData[Offset + 3] = Vert.Color.Red;
		MeshData[Offset + 4] = Vert.Color.Green;
		MeshData[Offset + 5] = Vert.Color.Blue;
		MeshData[Offset + 6] = Vert.Color.Alpha;
		MeshData[Offset + 7] = Vert.TexCoord[0];
		MeshData[Offset + 8] = Vert.TexCoord[1];

		Vertices++;
	}

	void DynamicMesh::Clear()
	{
		Vertices = 0;
	}

	void DynamicMesh::Upload()
	{
		VAO.Data(MeshData, Vertices * VERTEX_ELEMENTS);
	}
}
