#pragma once

#include "../../Core.h"
#include "../Shader.h"
#include "../Texture.h"
#include "../DynamicMesh.h"
#include "../Color.h"
#include "../../Math/Matrix.h"

#define MAX_VERTICES 50000

namespace Vega
{

	class VEGA_API SpriteBatch
	{
	public:
		SpriteBatch();
		~SpriteBatch();

		void Create();
		void Free();

		Vega::Color& GetTint();
		Vega::Mat4& GetProjectionMatrix();
		Vega::Mat4& GetViewMatrix();

		void SetTint(const Vega::Color& Tint);
		void SetProjectionMatrix(const Vega::Mat4& ProjectionMatrix);
		void SetViewMatrix(const Vega::Mat4& ProjectionMatrix);

		void Begin();
		void End();
		void Draw(const Vega::Texture& Texture, float X, float Y, float W, float H);

	private:

		void Flush();

		Vega::Shader* ColorShader;
		Vega::DynamicMesh Mesh;
		Vega::Color Tint;
		Vega::Mat4 ProjectionMatrix;
		Vega::Mat4 ViewMatrix;
		bool HasBegun;
		const Vega::Texture* Texture;

	};
}

