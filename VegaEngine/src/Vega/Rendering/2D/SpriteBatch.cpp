#include "SpriteBatch.h"
#include "../../Global.h"

namespace Vega
{

	SpriteBatch::SpriteBatch()
	{

	}

	SpriteBatch::~SpriteBatch()
	{

	}

	void SpriteBatch::Create()
	{
		ColorShader = Vega::Graphics::NewShader();
		ColorShader->CompileAndLink("./Shaders/Basic.vs", "./Shaders/Basic.fs");
		
		Mesh.Create(MAX_VERTICES);

		// Default projection matrix based on device viewport width and height.
		ProjectionMatrix.Ortho(0, static_cast<float>(Vega::Graphics::GetWidth()), 0, static_cast<float>(Vega::Graphics::GetHeight()), -0.01f, 100.0f);
		ViewMatrix.Identity();

		HasBegun = false;
	}

	void SpriteBatch::Free()
	{
		Mesh.Free();
	}

	Vega::Color& SpriteBatch::GetTint()
	{
		return Tint;
	}

	Vega::Mat4& SpriteBatch::GetProjectionMatrix()
	{
		return ProjectionMatrix;
	}

	Vega::Mat4& SpriteBatch::GetViewMatrix()
	{
		return ViewMatrix;
	}

	void SpriteBatch::SetTint(const Vega::Color& Tint)
	{
		this->Tint = Tint;
	}

	void SpriteBatch::SetProjectionMatrix(const Vega::Mat4& ProjectionMatrix)
	{
		this->ProjectionMatrix = ProjectionMatrix;
	}

	void SpriteBatch::SetViewMatrix(const Vega::Mat4& ViewMatrix)
	{
		this->ProjectionMatrix = ProjectionMatrix;
	}

	void SpriteBatch::Begin()
	{
		if (HasBegun)
			throw "SpriteBatch Begin() twice since End() was called";

		HasBegun = true;
		Texture = nullptr;
		Mesh.Clear();
	}

	void SpriteBatch::End()
	{
		if (!HasBegun)
			throw "End() called before Begin() was called";

		HasBegun = false;
		
		Flush();
	}

	void SpriteBatch::Flush()
	{
		ColorShader->UseProgram();
		ColorShader->SetUniformMat4f("ModelViewMatrix", ViewMatrix);
		ColorShader->SetUniformMat4f("ProjectionMatrix", ProjectionMatrix);

		Texture->Bind();

		Mesh.Upload();
		Mesh.Render();
	}

	void SpriteBatch::Draw(const Vega::Texture& Tex, float X, float Y, float W, float H)
	{

		if (!HasBegun)
			throw "SpriteBatch Draw() called before Begin()";

		// Check if the requested texture is different from the current texture.
		// If it is, flush the current load and start a new one.
		if(Texture != &Tex)
		{
			if (Texture)
				Flush();

			this->Texture = &Tex;
		}

		Vega::Vertex V1, V2, V3, V4;

		V1.Position = Vega::Vec3(X, Y, 0);
		V1.Color = Tint;
		V1.TexCoord = Vega::Vec2(0, 0);

		V2.Position = Vega::Vec3(X, Y + H, 0);
		V2.Color = Tint;
		V2.TexCoord = Vega::Vec2(0, 1);

		V3.Position = Vega::Vec3(X + W, Y + H, 0);
		V3.Color = Tint;
		V3.TexCoord = Vega::Vec2(1, 1);

		V4.Position = Vega::Vec3(X + W, Y, 0);
		V4.Color = Tint;
		V4.TexCoord = Vega::Vec2(1, 0);
		
		// Tri 1
		Mesh.AddVertex(V1);
		Mesh.AddVertex(V2);
		Mesh.AddVertex(V3);
		
		// Tri 2
		Mesh.AddVertex(V1);
		Mesh.AddVertex(V3);
		Mesh.AddVertex(V4);
	}
}

