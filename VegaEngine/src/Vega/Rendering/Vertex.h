#pragma once

#include "../Core.h"
#include "../Math/Vector.h"
#include "./Color.h"

#define VERTEX_ELEMENTS 9

namespace Vega
{
	typedef struct VEGA_API Vertex
	{
		Vega::Vec3 Position;
		Vega::Color Color;
		Vega::Vec2 TexCoord;

	} Vertex;
}

