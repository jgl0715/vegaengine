#include "VertexArray.h"
#include <iostream>

namespace Vega
{
	VertexArray::VertexArray()
	{

	}

	VertexArray::~VertexArray()
	{

	}

	void VertexArray::Create()
	{
		Vega::VertexAttrib PositionAttrib;
		Vega::VertexAttrib ColorAttrib;
		Vega::VertexAttrib TexCoordAttrib;
		std::vector<VertexAttrib> Attribs;

		PositionAttrib.Index = 0;
		PositionAttrib.Tuples = 3;
		ColorAttrib.Index = 1;
		ColorAttrib.Tuples = 4;
		TexCoordAttrib.Index = 2;
		TexCoordAttrib.Tuples = 2;

		Attribs.push_back(PositionAttrib);
		Attribs.push_back(ColorAttrib);
		Attribs.push_back(TexCoordAttrib);

		Create(Attribs);
	}

	void VertexArray::Create(const std::vector<Vega::VertexAttrib>& Attribs)
	{
		glGenVertexArrays(1, &Handle);
		Bind();

		DataBuffer.Create();

		int32 Stride = 0;
		uint32 Offset = 0;

		// Calculate stride.
		for (const Vega::VertexAttrib& Attrib : Attribs)
			Stride += sizeof(float) * Attrib.Tuples;

		// Specify alignment of vertex attributes to the vertex buffer
		// while also concurrently enabling the vertex attribute.
		for (const Vega::VertexAttrib& Attrib : Attribs)
		{
			glEnableVertexAttribArray(Attrib.Index);
			DataBuffer.Align(Attrib.Index, Attrib.Tuples, Stride, Offset);	
			Offset += Attrib.Tuples;
		}
	}

	void VertexArray::Bind()
	{
		glBindVertexArray(Handle);
	}

	void VertexArray::Data(float* Data, uint32 Size)
	{
		DataBuffer.SendData(Data, Size);
	}

	void VertexArray::Render(uint32 Vertices)
	{
		Bind();

		glDrawArrays(GL_TRIANGLES, 0, Vertices);
		
		glBindVertexArray(0);
	}

	void VertexArray::Free()
	{
		DataBuffer.Free();

		glDeleteVertexArrays(1, &Handle);
	}

	void VertexArray::SetUsage(GLenum Usage)
	{

	}

}
