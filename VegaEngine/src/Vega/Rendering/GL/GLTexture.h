#pragma once


#include "../../Core.h"
#include "../Image/Bitmap.h"
#include "../Texture.h"
#include "GL/glew.h"
#include <string>

namespace Vega
{
	class VEGA_API GLTexture : public Vega::Texture
	{
	public:
		GLTexture();
		virtual ~GLTexture();

		virtual void Bind() const override;
		virtual void LoadBmp(Vega::Bitmap& Bitmap);
		virtual void Free() override;

		virtual uint32 GetWidth() const;
		virtual uint32 GetHeight() const;

		virtual bool operator!=(const Texture* Other) const override;
		virtual bool operator==(const Texture* Other) const override;

		uint32 GetHandle() const;

	private:

		GLuint Handle;
	};
}


