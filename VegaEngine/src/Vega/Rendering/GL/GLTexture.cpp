#include "GLTexture.h"

namespace Vega
{
	GLTexture::GLTexture()
	{

	}

	GLTexture::~GLTexture()
	{

	}

	void GLTexture::Bind() const
	{
		glBindTexture(GL_TEXTURE_2D, Handle);
	}

	void GLTexture::LoadBmp(Vega::Bitmap& Bitmap)
	{
		glEnable(GL_TEXTURE_2D);

		// Set width and height of texture.
		this->Width = Bitmap.GetWidth();
		this->Height = Bitmap.GetHeight();

		// Create an OpenGL texture handle.
		glGenTextures(1, &Handle);

		// Bind the current texture before setting texture parameters.
		Bind();

		// Set the alignment of the pixel data to one byte.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		// Setup texture parameters.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// Send OpenGL the texture data depending on how many channels there were in the bitmap.
		switch (Bitmap.GetChannels())
		{
		case 4:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Bitmap.GetWidth(), Bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, Bitmap.GetData());
			break;
		case 3:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Bitmap.GetWidth(), Bitmap.GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, Bitmap.GetData());
			break;
		case 2:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, Bitmap.GetWidth(), Bitmap.GetHeight(), 0, GL_RG, GL_UNSIGNED_BYTE, Bitmap.GetData());
			break;
		case 1:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, Bitmap.GetWidth(), Bitmap.GetHeight(), 0, GL_RED, GL_UNSIGNED_BYTE, Bitmap.GetData());
			break;
		default:
			throw "Unsupported amount of channels in Bitmap!";
			break;
		}

	}

	void GLTexture::Free()
	{
		glDeleteTextures(1, &Handle);
	}

	uint32 GLTexture::GetWidth() const
	{
		return Width;
	}

	uint32 GLTexture::GetHeight() const
	{
		return Height;
	}

	uint32 GLTexture::GetHandle() const
	{
		return Handle;
	}

	bool GLTexture::operator!=(const Texture* Other) const
	{
		const Vega::GLTexture* GLTexture = dynamic_cast<const Vega::GLTexture*>(Other);
		if (GLTexture && Handle != GLTexture->GetHandle())
			return true;
		else
			return false;
	}
	
	bool GLTexture::operator==(const Texture* Other) const
	{
		const Vega::GLTexture* GLTexture = dynamic_cast<const Vega::GLTexture*>(Other);
		if (GLTexture && Handle == GLTexture->GetHandle())
			return true;
		else
			return false;

	}


}