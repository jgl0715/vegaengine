#pragma once

#include "GL/glew.h"
#include "../../Core.h"

namespace Vega
{

	class VEGA_API VertexBuffer
	{
	public:

		VertexBuffer();
		~VertexBuffer();

		void Create();
		void Bind();
		void SendData(float* Data, uint32 Size);
		void Align(uint32 Index, int32 Tuples, int32 Stride, uint32 Offset);
		void Free();
		void SetUsage(GLenum Usage);

	private:
		GLuint Handle;
		GLenum Usage;
	};
}


