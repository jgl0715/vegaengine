#include "GLShader.h"
#include "../../Core/Log.h"
#include <sstream>
#include <iostream>
#include <fstream>

namespace Vega
{

	GLShader::GLShader()
	{
	}

	GLShader::~GLShader()
	{
	}

	void GLShader::CompileAndLink(const std::string& VSPath, const std::string& FSPath)
	{
		this->Compiled = false;

		GLint LinkResult;
		GLint ValidateResult;

		GLsizei LogLength;
		GLchar InfoLog[1000];

		VG_LOG(LOG_INFO, "------ CREATING SHADER PROGRAM ------");
		ProgramHandle = glCreateProgram();

		if (ProgramHandle == 0)
		{
			VG_LOG(LOG_ERROR, "OpenGL failed to create a shader program handle.");
		}
		else
		{

			char VSCompileLog[1000];
			char FSCompileLog[1000];

			// Compile vertex and fragment shaders
			VG_LOG(LOG_INFO, "Compiling vertex shader.");
			VSHandle = CreateShader(VSPath, GL_VERTEX_SHADER, VSCompileLog);
			PrintShaderError(VSHandle, VSPath.c_str(), VSCompileLog, GL_VERTEX_SHADER);

			VG_LOG(LOG_INFO, "Compiling fragment shader.");
			FSHandle = CreateShader(FSPath, GL_FRAGMENT_SHADER, FSCompileLog);
			PrintShaderError(FSHandle, FSPath.c_str(), FSCompileLog, GL_FRAGMENT_SHADER);

			// Attach shaders to main program unit
			glAttachShader(ProgramHandle, VSHandle);
			glAttachShader(ProgramHandle, FSHandle);

			// Bind attrib locations
//			glBindAttribLocation(ProgramHandle, 0, "a_position");
//			glBindAttribLocation(ProgramHandle, 1, "a_color");
//			glBindAttribLocation(ProgramHandle, 2, "a_texcoord");

			// Link shader program.
			VG_LOG(LOG_INFO, "Linking shader program.");
			glLinkProgram(ProgramHandle);

			// Check for linking errors.
			glGetProgramiv(ProgramHandle, GL_LINK_STATUS, &LinkResult);
			if (LinkResult == GL_FALSE)
			{
				VG_LOG(LOG_ERROR, "Shader program failed to properly link.");
				glGetProgramInfoLog(ProgramHandle, 1000, &LogLength, InfoLog);
				throw InfoLog;
			}
			else
			{
				VG_LOG(LOG_INFO, "Shader program properly linked.");
			}

			// Validate shader program.
			VG_LOG(LOG_INFO, "Validating shader program.");
			glValidateProgram(ProgramHandle);

			// Check for validation errors.
			glGetProgramiv(ProgramHandle, GL_VALIDATE_STATUS, &ValidateResult);
			if (ValidateResult == GL_FALSE)
			{
				VG_LOG(LOG_ERROR, "Shader program failed validation.");
				glGetProgramInfoLog(ProgramHandle, 1000, &LogLength, InfoLog);
				throw InfoLog;
			}
			else
			{
				VG_LOG(LOG_INFO, "Shader program properly validated.");
				this->Compiled = true;
			}

			VG_LOG(LOG_INFO, "------ FINISHED SHADER PROGRAM ------");
		}

	}

	void GLShader::UseProgram()
	{
		glUseProgram(ProgramHandle);
	}

	bool GLShader::IsCompiled()
	{
		return Compiled;
	}

	void GLShader::SetUniformMat3f(const std::string& UniformName, Vega::Mat3& Matrix)
	{
		glUniformMatrix3fv(glGetUniformLocation(ProgramHandle, UniformName.c_str()), 1, true, Matrix.AsArray());
	}

	void GLShader::SetUniformMat4f(const std::string& UniformName, Vega::Mat4& Matrix)
	{
		glUniformMatrix4fv(glGetUniformLocation(ProgramHandle, UniformName.c_str()), 1, true, Matrix.AsArray());
	}

	std::string GLShader::ReadFileAsString(const std::string& FilePath)
	{
		std::stringstream FileStringStream;
		std::ifstream FileInput;
		std::string Line;

		FileInput.open(FilePath, std::ios::in);

		if (FileInput.is_open())
		{
			while (std::getline(FileInput, Line))
				FileStringStream << Line << '\n';

			return FileStringStream.str();
		}
		else
		{
			throw "File was not properly opened!";
			return "";
		}

	}

	void GLShader::PrintShaderError(GLuint ShaderHandle, const char* FilePath, char* Log, GLenum ShaderType)
	{
		if (ShaderHandle == 0)
		{
			VG_LOG(LOG_ERROR, "OpenGL failed to allocate a shader unit for the vertex shader.");
		}
		else if (ShaderHandle < 0)
		{
			switch (ShaderType)
			{
			case GL_VERTEX_SHADER:
				VG_LOG(LOG_ERROR, "------ VERTEX SHADER COMPILATION FAILURE ------");
				break;
			case GL_FRAGMENT_SHADER:
				VG_LOG(LOG_ERROR, "------ FRAGMENT SHADER COMPILATION FAILURE ------");
				break;
			}

			VG_LOGR(LOG_ERROR, "%s failed to compile:", FilePath);
			VG_LOGR(LOG_ERROR, "%s", Log);

			VG_LOG(LOG_ERROR, "------ END SHADER COMPILATION FAILURE ------");
		}
		else
		{
			switch (ShaderType)
			{
			case GL_VERTEX_SHADER:
				VG_LOG(LOG_INFO, "Vertex shader successfully compiled.");
				break;
			case GL_FRAGMENT_SHADER:
				VG_LOG(LOG_INFO, "Fragment shader successfully compiled.");
				break;
			}
		}
		
	}

	GLuint GLShader::CreateShader(const std::string& FilePath, GLenum ShaderType, char* CompileLog)
	{
		GLuint ShaderHandle = glCreateShader(ShaderType);
		std::string SourceCode = ReadFileAsString(FilePath);
		const char* SourceCode_CStr = SourceCode.c_str();
		GLint SourceCode_Len = (GLint) SourceCode.length();
		GLint CompilationResult;

		if (ShaderHandle > 0)
		{
			glShaderSource(ShaderHandle, 1, &SourceCode_CStr, &SourceCode_Len);
			glCompileShader(ShaderHandle);

			glGetShaderiv(ShaderHandle, GL_COMPILE_STATUS, &CompilationResult);

			if (CompilationResult == GL_FALSE)
			{

				// Shader shader handle to -1 to mark compilation failure
				ShaderHandle = -1;

				glGetShaderInfoLog(ShaderHandle, 1000, NULL, CompileLog);
				glDeleteShader(ShaderHandle);
			}
		}

		return ShaderHandle;
	}

}
