#include "VertexBuffer.h"

namespace Vega
{

	VertexBuffer::VertexBuffer()
	{
		Usage = GL_STATIC_DRAW;
	}

	VertexBuffer::~VertexBuffer()
	{
	}

	void VertexBuffer::Create()
	{
		glGenBuffers(1, &Handle);
	}

	void VertexBuffer::SendData(float* Data, uint32 Size)
	{
		Bind();
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * Size, Data, Usage);
	}

	void VertexBuffer::Bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, Handle);
	}

	void VertexBuffer::Align(uint32 Index, int32 Tuples, int32 Stride, uint32 Offset)
	{
		Bind();
		glVertexAttribPointer(Index, Tuples, GL_FLOAT, GL_FALSE, Stride, reinterpret_cast<void*>(sizeof(uint32) * Offset));
	}

	void VertexBuffer::Free()
	{
		glDeleteBuffers(1, &Handle);
	}

	void VertexBuffer::SetUsage(GLenum Usage)
	{
		this->Usage = Usage;
	}

}
