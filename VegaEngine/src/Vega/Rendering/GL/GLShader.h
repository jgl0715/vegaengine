#pragma once

#include "../../Core.h"
#include "../../Math/Matrix.h"
#include "../Shader.h"
#include <string>
#include "GL/glew.h"

namespace Vega
{


	class VEGA_API GLShader : public Vega::Shader
	{
	public:
		GLShader();
		~GLShader();

		virtual void CompileAndLink(const std::string& VSPath, const std::string& FSPath) override;
		virtual void UseProgram() override;
		virtual void SetUniformMat3f(const std::string& UniformName, Vega::Mat3& Matrix) override;
		virtual void SetUniformMat4f(const std::string& UniformName, Vega::Mat4& Matrix) override;

		bool IsCompiled();


	private:

		std::string ReadFileAsString(const std::string& FilePath);
		GLuint CreateShader(const std::string& FilePath, GLenum ShaderType, char* CompileLog);
		void PrintShaderError(GLuint ShaderHandle, const char* FilePath, char* Log, GLenum ShaderType);

		GLuint ProgramHandle;
		GLuint VSHandle;
		GLuint FSHandle;
		bool Compiled;
	};

}