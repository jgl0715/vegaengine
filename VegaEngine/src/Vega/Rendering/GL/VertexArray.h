#pragma once

#include "VertexBuffer.h"
#include <vector>

namespace Vega
{

	typedef struct VertexAttrib
	{
		uint32 Index;
		int32 Tuples;
	} VertexAttrib;

	class VEGA_API VertexArray
	{
	public:
		VertexArray();
		~VertexArray();

		void Create();
		void Create(const std::vector<VertexAttrib>& Attribs);
		void Bind();
		void Data(float* Data, uint32 Size);
		void Render(uint32 Vertices);
		void Free();
		void SetUsage(GLenum Usage);

	private:

		VertexBuffer DataBuffer;

		GLuint Handle;
	};

}
