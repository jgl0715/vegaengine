#include "Texture.h"

namespace Vega
{
	Texture::Texture()
	{

	}

	Texture::~Texture()
	{

	}
	
	void Texture::Load(const std::string& Path)
	{
		// Load a bitmap and pass it on to the convenience load function.
		Vega::Bitmap Bitmap;
		Bitmap.Load(Path);

		this->LoadBmp(Bitmap);
	}

	void Texture::LoadBmp(Vega::Bitmap& Bitmap)
	{

	}
}

