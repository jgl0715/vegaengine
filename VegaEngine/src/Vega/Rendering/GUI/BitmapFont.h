#pragma once

#include "../../Core.h"
#include "../../Resource/Resource.h"

namespace Vega
{
	
	class SpriteBatch;
	class Texture;

	struct FontGlyph
	{
		int32 CharY;
		int32 CharX;
		int32 CharWidth;
		int32 CharHeight;
	};

	class VEGA_API BitmapFont : public Vega::Resource
	{
	public:
		BitmapFont();
		virtual ~BitmapFont();

		virtual void Load(const std::string& Path);
		virtual void Free();

		void Draw(const std::string Text, Vega::SpriteBatch* Batch, float X, float Y);

	private:

		FontGlyph Glyphs[256];

		Vega::Texture* BitmapTexture;
	};
}

