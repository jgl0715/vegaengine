#include "BitmapFont.h"

#include "../../Global.h"
#include "../2D/SpriteBatch.h"
#include "../../Resource/ResourceManager.h"
#include <fstream>

namespace Vega
{
	BitmapFont::BitmapFont()
	{
	}

	BitmapFont::~BitmapFont()
	{
	}

	void BitmapFont::Load(const std::string& Path)
	{
		std::ifstream AtlasInput;
		AtlasInput.open(Path, std::ios_base::in);

		std::string TextureFile;
		std::string FontName;
		int32 CharacterHeight;
		int32 Glyph = 31;

		AtlasInput >> TextureFile;
		AtlasInput >> FontName;
		AtlasInput >> CharacterHeight;

		while (AtlasInput)
		{
			AtlasInput >> Glyphs[Glyph].CharX;
			AtlasInput >> Glyphs[Glyph].CharY;
			AtlasInput >> Glyphs[Glyph].CharWidth;
			Glyphs[Glyph].CharHeight = CharacterHeight;

			Glyph++;
		}

		AtlasInput.close();

		BitmapTexture = Vega::Res::Man->GetTexture(Path);
	}

	void BitmapFont::Free()
	{
	}

	void BitmapFont::Draw(const std::string Text, Vega::SpriteBatch* Batch, float X, float Y)
	{
		Batch->Draw(*BitmapTexture, X, Y, static_cast<float>(BitmapTexture->GetWidth()), static_cast<float>(BitmapTexture->GetHeight()));
	}

}
