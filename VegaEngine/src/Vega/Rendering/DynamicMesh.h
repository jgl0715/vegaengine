#pragma once

#include "../Core.h"
#include "GL/VertexArray.h"
#include "Vertex.h"

namespace Vega
{
	class VEGA_API DynamicMesh
	{
	public:
		DynamicMesh();
		~DynamicMesh();

		void Create(uint32 MaxVertices);
		void Free();
		void Render();
		void Clear();

		void AddVertex(Vega::Vertex& Vert);
		void Upload();

	private:
		
		// OpenGL specific implementation
		// TODO: abstract rendering interface
		VertexArray VAO;

		uint32 MaxVertices;
		uint32 Vertices;
		float* MeshData;
	};

}
