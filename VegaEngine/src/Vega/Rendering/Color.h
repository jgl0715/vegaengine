#pragma once

#include "../Core.h"

namespace Vega
{

	typedef struct VEGA_API Color
	{
		float Alpha;
		float Red;
		float Green;
		float Blue;

		Color::Color()
		{
			Alpha = 1.0f;
			Red = 0.0f;
			Green = 0.0f;
			Blue = 0.0f;
		}

		Color::Color(uint8 A, uint8 R, uint8 G, uint8 B)
		{
			Set((float)A / 255.0f, (float)R / 255.0f, (float)G / 255.0f, (float)B / 255.0f);
		}

		void Set(float A, float R, float G, float B)
		{
			Alpha = A;
			Red = R;
			Green = G;
			Blue = B;
		}

	} Color;


	const Vega::Color BLACK(255, 0, 0, 0);
	const Vega::Color WHITE(255, 255, 255, 255);
	const Vega::Color RED(255, 255, 0, 0);
	const Vega::Color GREEN(255, 0, 255, 0);
	const Vega::Color BLUE(255, 0, 0, 255);

}
