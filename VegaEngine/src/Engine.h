#pragma once

#include "Vega/Core/ApplicationListener.h"
#include "Vega/Core/Log.h"
#include "Vega/Resource/ResourceManager.h"
#include "Vega/Math/VMath.h"
#include "Vega/PlatformEntry.h"
#include "Vega/Global.h"
#include "Vega/Rendering/GL/GLTexture.h"
#include "Vega/Rendering/DynamicMesh.h"
#include "Vega/Rendering/2D/SpriteBatch.h"
#include "Vega/Rendering/GUI/BitmapFont.h"