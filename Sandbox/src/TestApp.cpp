#include "Engine.h"

class TestApp : public Vega::ApplicationListener
{
public:

	TestApp()
	{

	}

	~TestApp()
	{

	}

	void TestApp::Update()
	{
		X += 0.01f;
		ModelViewMatrix.Translation(X, X, 0.0f);
	}

	void TestApp::Render()
	{
		Batch.Begin();
		Batch.Draw(*Texture, 0, 0, 100, 100);
		Batch.Draw(*Texture, 100, 100, 100, 100);
		Batch.Draw(*Texture, 200, 200, 100, 100);
		Batch.End();
	}

	void TestApp::Init()
	{
		// Load texture
		Texture = Vega::Res::Man->GetTexture("./Textures/Tiles.bmp");
		BitmapFont = Vega::Res::Man->GetFont("./Fonts/out.png");

		Batch.Create();

		X = 0.0f;
	}

private:

	Vega::Texture* Texture;
	Vega::BitmapFont* BitmapFont;
	Vega::Mat4 ModelViewMatrix;
	Vega::SpriteBatch Batch;
	float X;

};

Vega::ApplicationListener* Vega::CreateApplicationListener()
{
	// Can be used to run different games.
	return new TestApp;
}