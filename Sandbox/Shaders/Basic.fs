#version 330

uniform sampler2D sampler;

in vec4 v_color;
in vec2 v_texcoord;

out vec4 Color;

void main()
{
	Color = texture(sampler, v_texcoord);
}